package br.com.system.managers;

import br.com.system.errors.Errors;
import br.com.system.helpers.LogHelper;
import br.com.system.helpers.ObjectMapperHelper;

public class Manager {

	private ObjectMapperHelper mapperHelper;

	public Manager(){
		this.mapperHelper = new ObjectMapperHelper();
	}

	@SuppressWarnings("unchecked")
	protected <T> T mapParams(String params, Class<?> className){
		try {
			return (T) this.mapperHelper.getMapper().readValue(params, className);
		} catch (Exception e) {
			LogHelper.error(e);
			return null;
		}
	}

	protected String parseResponse(Object object){
		try{
			return this.mapperHelper.getMapper().writeValueAsString(object);
		} catch (Exception e) {
			LogHelper.error(e);
			return Errors.getError(Errors.RESPONSE_PARSE_FAIL);
		}
	}
}
