package br.com.system.i18n;

import java.io.File;

import org.apache.commons.io.FileUtils;

import br.com.system.Conf;

import com.amazonaws.util.json.JSONObject;

public class Strings {
	
	public static JSONObject STRINGS;
	
	synchronized public static String get(String key){
		Strings.openStringsFile();
		if(Strings.STRINGS == null){
			return "";
		}
		
		try{
			return Strings.STRINGS.getString(key);
		}catch(Exception e){
			return "";
		}
	}
	
	private static void openStringsFile(){
		try {
			if(Strings.STRINGS != null){
				return;
			}
			
			File f = new File(Conf.I18N_PATH + "strings-pt-br.js");
			String data = FileUtils.readFileToString(f, "UTF-8");
			Strings.STRINGS = new JSONObject(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
