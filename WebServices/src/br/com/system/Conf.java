package br.com.system;

public class Conf {

	public static final String DATABASE_CONNECTION_STRING = "localhost";

	//	public static final String DATABASE_HOST = "ec2-54-210-131-63.compute-1.amazonaws.com";
	public static final String DATABASE_HOST = "localhost";
	public static final String DATABASE_PORT = "3306";
	public static final String DATABASE_NAME = "mototaxis";
	public static final String DATABASE_USER = "root";
	public static final String DATABASE_PASSWORD = "a1b2@Mateus";
	//	public static final String DATABASE_USER = "admin_db";
	//	public static final String DATABASE_PASSWORD = "p@ssw0rd";

	public static final int EMAIL_SMTP_PORT = 587;
	public static final String EMAIL_SMTP_HOST_NAME = "smtp.gmail.com";
	public static final String EMAIL_SMTP_SSL_PORT = "465";
	public static final String EMAIL_SMTP_SENDER_MAIL = "email@email.com.br";
	public static final String EMAIL_SMTP_SENDER_NAME = "Sender Name";
	public static final String EMAIL_SMTP_SENDER_PASSWORD = "password";

	public static final String EMAIL_POP_HOST_NAME = "pop.gmail.com";
	public static final String EMAIL_POP_USERNAME = "email@email.com.br";
	public static final String EMAIL_POP_PASSWORD = "password";
	public static final String EMAIL_POP_PROTOCOL = "imaps";
	public static final String EMAIL_POP_DEFAULT_FOLDER = "INBOX";

	public static final String BASE_PATH = "//var//www//html//files//";
	public static final String TEMP_PATH = Conf.BASE_PATH + "fotos//";
	public static final String I18N_PATH = Conf.BASE_PATH + "i18n//";

	public static final String AWS_S3_BUCKET_IMAGENS = "system-images";

	public static final String GOOGLE_CGM_APP_ID = "google-gcm-app-id";
}
