package br.com.system.security;

import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;

public class Encrypter {
	
	private static final String	KEY			= "a2554e5374ea40ffe2dba27c";
	private static final String	IV_STRING	= "46874551";
	
	private KeySpec				keySpec;
	private SecretKey			key;
	private IvParameterSpec		iv;
	
	public Encrypter() throws Exception {
		this.init(Encrypter.KEY);
	}
	
	public Encrypter(final String key) throws Exception {
		this.init(key);
	}
	
	private void init(final String keyString) throws Exception {
		try {
			final MessageDigest md = MessageDigest.getInstance("md5");
			final byte[] digestOfPassword = md.digest(Base64.decodeBase64(keyString.getBytes("utf-8")));
			final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			for (int j = 0, k = 16; j < 8;) {
				keyBytes[k++] = keyBytes[j++];
			}
			this.keySpec = new DESedeKeySpec(keyBytes);
			this.key = SecretKeyFactory.getInstance("DESede").generateSecret(this.keySpec);
			this.iv = new IvParameterSpec(Encrypter.IV_STRING.getBytes());
		} catch (final Exception e) {
			throw e;
		}
	}
	
	public String encrypt(final String value) throws Exception {
		try {
			final Cipher ecipher = Cipher.getInstance("DESede/CBC/PKCS5Padding", "SunJCE");
			ecipher.init(Cipher.ENCRYPT_MODE, this.key, this.iv);
			if (value == null) {
				return null;
			}
			final byte[] utf8 = value.getBytes("UTF8");
			final byte[] enc = ecipher.doFinal(utf8);
			return new String(Base64.encodeBase64(enc), "UTF-8");
		} catch (final Exception e) {
			throw e;
		}
	}
	
	public String decrypt(final String value) throws Exception {
		try {
			final Cipher dcipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			dcipher.init(Cipher.DECRYPT_MODE, this.key, this.iv);
			if (value == null) {
				return null;
			}
			final byte[] dec = Base64.decodeBase64(value.getBytes());
			final byte[] utf8 = dcipher.doFinal(dec);
			return new String(utf8, "UTF8");
		} catch (final Exception e) {
			throw e;
		}
	}
}