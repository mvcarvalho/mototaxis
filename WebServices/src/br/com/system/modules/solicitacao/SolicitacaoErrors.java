package br.com.system.modules.solicitacao;

public class SolicitacaoErrors {

	public static final int SOLICITACAO_PARSE_FAIL = 30001;
	public static final int SOLICITACAO_NOT_FOUND = 30002;
	public static final int SOLICITACAO_ALREADY_ACCEPTED = 30003;
	public static final int SOLICITACAO_NOT_ACCEPTED = 30004;
	public static final int NO_NEW_SOLICITACAO = 30005;

	public static String getError(int error){
		switch (error) {
		case SOLICITACAO_PARSE_FAIL:
			return "{\"error\":\"Falha ao mapear par�metros da solicita��o.\", \"errorcode\":\"30001\"}";

		case SOLICITACAO_NOT_FOUND:
			return "{\"error\":\"A solicita��o n�o foi encontrada.\", \"errorcode\":\"30002\"}";

		case SOLICITACAO_ALREADY_ACCEPTED:
			return "{\"error\":\"A solicita��o j� foi aceita por outro agente.\", \"errorcode\":\"30003\"}";

		case SOLICITACAO_NOT_ACCEPTED:
			return "{\"error\":\"A solicita��o ainda n�o foi aceita.\", \"errorcode\":\"30004\"}";

		case NO_NEW_SOLICITACAO:
			return "{\"error\":\"N�o foi encontrado nenhuma solicita��o.\", \"errorcode\":\"30005\"}";

		default:
			return "{\"error\":\"Erro desconhecido.\", \"errorcode\":\"00000\"}";
		}
	}
}
