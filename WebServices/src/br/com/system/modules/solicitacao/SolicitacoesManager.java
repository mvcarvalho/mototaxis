package br.com.system.modules.solicitacao;

import java.util.ArrayList;
import java.util.UUID;

import br.com.system.helpers.DatabaseHelper;
import br.com.system.managers.Manager;

public class SolicitacoesManager extends Manager{

	public String recuperarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(solicitacao, "guid = '" + solicitacao.getGuid() + "'");

		if(solicitacao.getId() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		return this.parseResponse(solicitacao);
	}

	@SuppressWarnings("unchecked")
	public String listarSolicitacoes(){
		DatabaseHelper databaseHelper = new DatabaseHelper();
		ArrayList<Solicitacao> solicitacoes = (ArrayList<Solicitacao>) databaseHelper.queryAll(new Solicitacao(), "id > 0");

		if(solicitacoes.size() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		return this.parseResponse(solicitacoes);
	}

	@SuppressWarnings("unchecked")
	public String recuperarSolicitacaoAberto(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		StringBuilder sql = new StringBuilder("status = '" + solicitacao.getStatus() + "'");

		if((solicitacao.getLatitude() != 0) && (solicitacao.getLongitude() != 0)){
			sql.append(" AND latitude > " + (solicitacao.getLatitude() - 0.02));
			sql.append(" AND latitude < " + (solicitacao.getLatitude() + 0.02));
			sql.append(" AND longitude > " + (solicitacao.getLongitude() - 0.02));
			sql.append(" AND longitude < " + (solicitacao.getLongitude() + 0.02));
		}

		if((solicitacao.getTipo() != null) && !solicitacao.getTipo().equals("") ){
			sql.append(" AND tipo = '" + solicitacao.getTipo() + "'");
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		ArrayList<Solicitacao> solicitacoes = (ArrayList<Solicitacao>) databaseHelper.queryAll(solicitacao, sql.toString());

		if((solicitacoes == null) || (solicitacoes.size() <= 0)){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		return this.parseResponse(solicitacoes.get(0));
	}

	public String iniciarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		solicitacao.setGuid(UUID.randomUUID().toString());
		solicitacao.setStatus("A");
		solicitacao.setData(System.currentTimeMillis());

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.insert(solicitacao);
		databaseHelper.query(solicitacao, "guid = '" + solicitacao.getGuid() + "'");

		//TODO
		//Adicionar envio da mensagem aos aplicativos de mototaxistas

		return this.parseResponse(solicitacao);
	}

	public String aceitarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		Solicitacao solicitacaoBanco = new Solicitacao();
		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(solicitacaoBanco, "guid = '" + solicitacao.getGuid() + "'");

		if(solicitacaoBanco.getId() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		if(!solicitacaoBanco.getStatus().equals("A")){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_ALREADY_ACCEPTED);
		}

		solicitacaoBanco.setIdagente(solicitacao.getIdagente());
		solicitacaoBanco.setStatus("I");
		databaseHelper.update(solicitacaoBanco);

		//TODO
		//Adicionar envio da mensagem aos aplicativos de mototaxistas e do usu�rio que solicitou

		return this.parseResponse(solicitacaoBanco);
	}

	public String confirmarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(solicitacao, "guid = '" + solicitacao.getGuid() + "'");

		if(solicitacao.getId() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		if(solicitacao.getStatus().equals("A")){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_ACCEPTED);
		}

		solicitacao.setStatus("E");
		databaseHelper.update(solicitacao);

		//TODO
		//Adicionar envio da mensagem ao aplicativo de mototaxista

		return this.parseResponse(solicitacao);
	}

	public String cancelarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(solicitacao, "guid = '" + solicitacao.getGuid() + "'");

		if(solicitacao.getId() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		SolicitacaoEncerrada solicitacaoEncerrada = new SolicitacaoEncerrada(solicitacao);
		solicitacaoEncerrada.setStatus("C");
		solicitacaoEncerrada.setDataencerramento(System.currentTimeMillis());

		databaseHelper.insert(solicitacaoEncerrada);
		databaseHelper.delete(solicitacao);

		//TODO
		//Adicionar envio da mensagem ao aplicativo de mototaxista

		return this.parseResponse(solicitacaoEncerrada);
	}

	public String encerrarSolicitacao(String params){
		Solicitacao solicitacao = this.mapParams(params, Solicitacao.class);

		if(solicitacao == null) {
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(solicitacao, "guid = '" + solicitacao.getGuid() + "'");

		if(solicitacao.getId() <= 0){
			return SolicitacaoErrors.getError(SolicitacaoErrors.SOLICITACAO_NOT_FOUND);
		}

		SolicitacaoEncerrada solicitacaoEncerrada = new SolicitacaoEncerrada(solicitacao);
		solicitacaoEncerrada.setStatus("F");
		solicitacaoEncerrada.setDataencerramento(System.currentTimeMillis());

		databaseHelper.insert(solicitacaoEncerrada);
		databaseHelper.delete(solicitacao);

		//TODO
		//Adicionar envio da mensagem ao aplicativo de mototaxista e do usu�rio

		return this.parseResponse(solicitacaoEncerrada);
	}
}
