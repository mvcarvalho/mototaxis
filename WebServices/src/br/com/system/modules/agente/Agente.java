package br.com.system.modules.agente;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
	"nome",
	"email",
	"login",
	"senha",
	"latitude",
	"longitude",
	"precoviagem",
	"taxaviagem",
	"alvara",
	"telefone1",
	"telefone2",
	"modelomoto",
	"cormoto",
	"placamoto",
	"foto",
	"modelocarro",
	"corcarro",
	"placacarro",
	"tipo"
})
public class Agente {

	@JsonProperty("id")
	private int id;
	@JsonProperty("nome")
	private String nome;
	@JsonProperty("email")
	private String email;
	@JsonProperty("login")
	private String login;
	@JsonProperty("senha")
	private String senha;
	@JsonProperty("latitude")
	private double latitude;
	@JsonProperty("longitude")
	private double longitude;
	@JsonProperty("precoviagem")
	private double precoviagem;
	@JsonProperty("taxaviagem")
	private double taxaviagem;
	@JsonProperty("alvara")
	private String alvara;
	@JsonProperty("telefone1")
	private String telefone1;
	@JsonProperty("telefone2")
	private String telefone2;
	@JsonProperty("modelomoto")
	private String modelomoto;
	@JsonProperty("cormoto")
	private String cormoto;
	@JsonProperty("placamoto")
	private String placamoto;
	@JsonProperty("cidade")
	private String cidade;
	@JsonProperty("estado")
	private String estado;
	@JsonProperty("endereco")
	private String endereco;
	@JsonProperty("cpf")
	private String cpf;
	@JsonProperty("rg")
	private String rg;
	@JsonProperty("foto")
	private String foto;
	@JsonProperty("modelocarro")
	private String modelocarro;
	@JsonProperty("corcarro")
	private String corcarro;
	@JsonProperty("placacarro")
	private String placacarro;
	@JsonProperty("tipo")
	private String tipo;

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The nome
	 */
	@JsonProperty("nome")
	public String getNome() {
		return this.nome;
	}

	/**
	 * 
	 * @param nome
	 *            The nome
	 */
	@JsonProperty("nome")
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * 
	 * @return The email
	 */
	@JsonProperty("email")
	public String getEmail() {
		return this.email;
	}

	/**
	 * 
	 * @param email
	 *            The email
	 */
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return The login
	 */
	@JsonProperty("login")
	public String getLogin() {
		return this.login;
	}

	/**
	 * 
	 * @param login
	 *            The login
	 */
	@JsonProperty("login")
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * 
	 * @return The senha
	 */
	@JsonProperty("senha")
	public String getSenha() {
		return this.senha;
	}

	/**
	 * 
	 * @param senha
	 *            The senha
	 */
	@JsonProperty("senha")
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * 
	 * @return The latitude
	 */
	@JsonProperty("latitude")
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * 
	 * @param latitude
	 *            The latitude
	 */
	@JsonProperty("latitude")
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * 
	 * @return The longitude
	 */
	@JsonProperty("longitude")
	public double getLongitude() {
		return this.longitude;
	}

	/**
	 * 
	 * @param longitude
	 *            The longitude
	 */
	@JsonProperty("longitude")
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 
	 * @return The precoviagem
	 */
	@JsonProperty("precoviagem")
	public double getPrecoviagem() {
		return this.precoviagem;
	}

	/**
	 * 
	 * @param precoviagem
	 *            The precoviagem
	 */
	@JsonProperty("precoviagem")
	public void setPrecoviagem(double precoviagem) {
		this.precoviagem = precoviagem;
	}

	/**
	 * 
	 * @return The taxaviagem
	 */
	@JsonProperty("taxaviagem")
	public double getTaxaviagem() {
		return this.taxaviagem;
	}

	/**
	 * 
	 * @param taxaviagem
	 *            The taxaviagem
	 */
	@JsonProperty("taxaviagem")
	public void setTaxaviagem(double taxaviagem) {
		this.taxaviagem = taxaviagem;
	}

	@JsonProperty("alvara")
	public String getAlvara() {
		return this.alvara;
	}

	@JsonProperty("alvara")
	public void setAlvara(String alvara) {
		this.alvara = alvara;
	}

	@JsonProperty("telefone1")
	public String getTelefone1() {
		return this.telefone1;
	}

	@JsonProperty("telefone1")
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	@JsonProperty("telefone2")
	public String getTelefone2() {
		return this.telefone2;
	}

	@JsonProperty("telefone2")
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	@JsonProperty("modelomoto")
	public String getModelomoto() {
		return this.modelomoto;
	}

	@JsonProperty("modelomoto")
	public void setModelomoto(String modelomoto) {
		this.modelomoto = modelomoto;
	}

	@JsonProperty("cormoto")
	public String getCormoto() {
		return this.cormoto;
	}

	@JsonProperty("cormoto")
	public void setCormoto(String cormoto) {
		this.cormoto = cormoto;
	}

	@JsonProperty("placamoto")
	public String getPlacamoto() {
		return this.placamoto;
	}

	@JsonProperty("placamoto")
	public void setPlacamoto(String placamoto) {
		this.placamoto = placamoto;
	}

	@JsonProperty("cidade")
	public String getCidade() {
		return this.cidade;
	}

	@JsonProperty("cidade")
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@JsonProperty("estado")
	public String getEstado() {
		return this.estado;
	}

	@JsonProperty("estado")
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@JsonProperty("endereco")
	public String getEndereco() {
		return this.endereco;
	}

	@JsonProperty("endereco")
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@JsonProperty("cpf")
	public String getCpf() {
		return this.cpf;
	}

	@JsonProperty("cpf")
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@JsonProperty("rg")
	public String getRg() {
		return this.rg;
	}

	@JsonProperty("rg")
	public void setRg(String rg) {
		this.rg = rg;
	}

	@JsonProperty("foto")
	public String getFoto() {
		return this.foto;
	}

	@JsonProperty("foto")
	public void setFoto(String foto) {
		this.foto = foto;
	}

	@JsonProperty("modelocarro")
	public String getModelocarro() {
		return this.modelocarro;
	}

	@JsonProperty("modelocarro")
	public void setModelocarro(String modelocarro) {
		this.modelocarro = modelocarro;
	}

	@JsonProperty("corcarro")
	public String getCorcarro() {
		return this.corcarro;
	}

	@JsonProperty("corcarro")
	public void setCorcarro(String corcarro) {
		this.corcarro = corcarro;
	}

	@JsonProperty("placacarro")
	public String getPlacacarro() {
		return this.placacarro;
	}

	@JsonProperty("placacarro")
	public void setPlacacarro(String placacarro) {
		this.placacarro = placacarro;
	}

	@JsonProperty("tipo")
	public String getTipo() {
		return this.tipo;
	}

	@JsonProperty("tipo")
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}