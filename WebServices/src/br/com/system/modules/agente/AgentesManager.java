package br.com.system.modules.agente;

import java.util.ArrayList;

import br.com.system.helpers.DatabaseHelper;
import br.com.system.managers.Manager;

public class AgentesManager extends Manager {

	@SuppressWarnings("unchecked")
	public String listarAgentes(String params){
		Agente agente = this.mapParams(params, Agente.class);

		String sql;
		if(agente != null){
			sql = "tipo = '" + agente.getTipo() + "'";
		}else{
			sql = "id > 0";
		}

		ArrayList<Agente> agentes = null;
		DatabaseHelper databaseHelper = new DatabaseHelper();
		agentes = (ArrayList<Agente>) databaseHelper.queryAll(new Agente(), sql);

		if(agentes == null){
			return AgenteErrors.getError(AgenteErrors.AGENTS_NOT_FOUND);
		}
		return this.parseResponse(agentes);
	}

	public String efetuarLogin(String params){
		Agente agente = this.mapParams(params, Agente.class);

		if(agente == null){
			return AgenteErrors.getError(AgenteErrors.AGENT_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(agente, "login = '" + agente.getLogin() + "' AND senha = '" + agente.getSenha() + "'");

		if((agente == null) || (agente.getId() <= 0)){
			return AgenteErrors.getError(AgenteErrors.AGENT_LOGIN_FAIL);
		}
		return this.parseResponse(agente);
	}

	public String recuperarAgente(String params){
		Agente agente = this.mapParams(params, Agente.class);

		if(agente == null){
			return AgenteErrors.getError(AgenteErrors.AGENT_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();
		databaseHelper.query(agente, "id = " + agente.getId());

		if((agente == null) || (agente.getId() <= 0)){
			return AgenteErrors.getError(AgenteErrors.AGENTS_NOT_FOUND);
		}
		return this.parseResponse(agente);
	}

	public String salvarAgente(String params){
		Agente agente = this.mapParams(params, Agente.class);

		if(agente == null){
			return AgenteErrors.getError(AgenteErrors.AGENT_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();

		if(agente.getId() > 0){
			databaseHelper.update(agente);
		}else{
			Agente agenteBanco = new Agente();
			databaseHelper.query(agenteBanco, "login = '" + agente.getLogin() +"'");

			if((agenteBanco != null) && (agenteBanco.getId()> 0)){
				return AgenteErrors.getError(AgenteErrors.LOGIN_ALREADY_REGISTERED);
			}

			databaseHelper.insert(agente);
			databaseHelper.query(agente, "login = '" + agente.getLogin() + "'");
		}

		return this.parseResponse(agente);
	}

	public String salvarPosicaoAgente(String params){
		Agente agente = this.mapParams(params, Agente.class);

		if(agente == null){
			return AgenteErrors.getError(AgenteErrors.AGENT_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();

		if(agente.getId() > 0){
			Agente agenteBanco = new Agente();
			databaseHelper.query(agenteBanco, "id = " + agente.getId());

			if((agenteBanco == null) || (agenteBanco.getId() <= 0)){
				return AgenteErrors.getError(AgenteErrors.AGENTS_NOT_FOUND);
			}

			agenteBanco.setLatitude(agente.getLatitude());
			agenteBanco.setLongitude(agente.getLongitude());

			databaseHelper.update(agenteBanco);

			return this.parseResponse(agenteBanco);
		}else{
			return AgenteErrors.getError(AgenteErrors.AGENTS_NOT_FOUND);
		}
	}
}
