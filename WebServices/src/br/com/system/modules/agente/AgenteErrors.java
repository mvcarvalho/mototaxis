package br.com.system.modules.agente;

public class AgenteErrors {

	public static final int AGENT_PARSE_FAIL = 20001;
	public static final int AGENT_LOGIN_FAIL = 20002;
	public static final int LOGIN_ALREADY_REGISTERED = 20003;
	public static final int AGENTS_NOT_FOUND = 20004;

	public static String getError(int error){
		switch (error) {
		case AGENT_PARSE_FAIL:
			return "{\"error\":\"Falha ao mapear par�metros do agente.\", \"errorcode\":\"20001\"}";

		case AGENT_LOGIN_FAIL:
			return "{\"error\":\"Autentica��o de agente falhou. Verifique login e senha.\", \"errorcode\":\"20002\"}";

		case LOGIN_ALREADY_REGISTERED:
			return "{\"error\":\"Este login j� est� cadastrado.\", \"errorcode\":\"20003\"}";

		case AGENTS_NOT_FOUND:
			return "{\"error\":\"N�o foram encontrados agentes.\", \"errorcode\":\"20004\"}";

		default:
			return "{\"error\":\"Erro desconhecido.\", \"errorcode\":\"00000\"}";
		}
	}
}
