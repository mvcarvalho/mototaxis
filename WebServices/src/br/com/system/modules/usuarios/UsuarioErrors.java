package br.com.system.modules.usuarios;

public class UsuarioErrors {

	public static final int USUARIO_PARSE_FAIL = 20001;
	public static final int USUARIO_ALREADY_REGISTERED = 20002;
	public static final int USUARIO_NOT_FOUND = 20004;

	public static String getError(int error){
		switch (error) {
		case USUARIO_PARSE_FAIL:
			return "{\"error\":\"Falha ao mapear par�metros do usu�rio.\", \"errorcode\":\"20001\"}";

		case USUARIO_ALREADY_REGISTERED:
			return "{\"error\":\"Este usu�rio j� est� registrado.\", \"errorcode\":\"20002\"}";

		case USUARIO_NOT_FOUND:
			return "{\"error\":\"O usu�rio n�o foi encontrado.\", \"errorcode\":\"20004\"}";

		default:
			return "{\"error\":\"Erro desconhecido.\", \"errorcode\":\"00000\"}";
		}
	}
}
