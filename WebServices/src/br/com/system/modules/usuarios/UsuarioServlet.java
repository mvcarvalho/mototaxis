package br.com.system.modules.usuarios;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.system.entities.Request;
import br.com.system.errors.Errors;
import br.com.system.helpers.LogHelper;
import br.com.system.helpers.ObjectMapperHelper;
import br.com.system.helpers.RequestsHelper;

@WebServlet(name = "UsuarioServlet", urlPatterns = { "/usuario", "/usuarioservlet" })
public class UsuarioServlet extends HttpServlet{

	private static final long	serialVersionUID	= -2575151941375871600L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		new RequestsHelper().buildResponse(resp, "Solicita��es GET n�o permitidas.");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		this.processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp){
		/**
		 * TODO
		 * Processar os dados da request e utilizar Criptografia.
		 */

		RequestsHelper helper = null;
		//		Encrypter encrypter = null;
		String responseString = null;
		String requestData = null;

		helper = new RequestsHelper();
		requestData = helper.readRequest(req);

		//		try {
		//			encrypter = new Encrypter();
		//		} catch (Exception e) {
		//			LogHelper.error(e);
		//			responseString = Errors.getError(Errors.ENCRIPTOR_ERROR);
		//		}

		//		try {
		//			requestData = encrypter.decrypt(requestData);
		//		} catch (Exception e) {
		//			LogHelper.error(e);
		//			responseString = Errors.getError(Errors.DECRIPT_ERROR);
		//		}

		//		if(responseString != null) {
		responseString = this.processParams(requestData);
		//		}

		//		try {
		//			responseString = encrypter.encrypt(responseString);
		//		} catch (Exception e) {
		//			LogHelper.error(e);
		//			responseString = Errors.getError(Errors.ENCRIPT_ERROR);
		//		}

		try{
			helper.buildResponse(resp, responseString);
		}catch(Exception e){
			LogHelper.error(e);
		}
	}

	private String processParams(String params){
		if(params == null){
			return Errors.getError(Errors.INVALID_PARAMS);
		}

		ObjectMapperHelper helper = null;
		Request request = new Request();
		UsuariosManager manager = null;

		helper = new ObjectMapperHelper();

		try {
			request = helper.getMapper().readValue(params, Request.class);
		} catch (Exception e) {
			LogHelper.error(e);
			return Errors.getError(Errors.REQUEST_PARSE_FAIL);
		}

		manager = new UsuariosManager();

		if(request.getMethod().equals("recuperarUsuario")){
			return manager.recuperarUsuario(request.getParams());

		} else if(request.getMethod().equals("salvarUsuario")){
			return manager.salvarUsuario(request.getParams());

		}else{
			return Errors.getError(Errors.METHOD_NOT_FOUND);
		}
	}
}
