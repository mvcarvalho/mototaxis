package br.com.system.modules.usuarios;

import br.com.system.helpers.DatabaseHelper;
import br.com.system.managers.Manager;

public class UsuariosManager extends Manager {

	public String recuperarUsuario(String params){
		Usuario usuario = this.mapParams(params, Usuario.class);

		if(usuario == null){
			return UsuarioErrors.getError(UsuarioErrors.USUARIO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();

		if(usuario.getId() > 0){
			databaseHelper.query(usuario, "id = " + usuario.getId());
		}else if((usuario.getEmail() != null) && !usuario.getEmail().equals("")){
			databaseHelper.query(usuario, "email = '" + usuario.getEmail() +"'");
		}

		if((usuario == null) || (usuario.getId() <= 0)){
			return UsuarioErrors.getError(UsuarioErrors.USUARIO_NOT_FOUND);
		}
		return this.parseResponse(usuario);
	}

	public String salvarUsuario(String params){
		Usuario usuario = this.mapParams(params, Usuario.class);

		if(usuario == null){
			return UsuarioErrors.getError(UsuarioErrors.USUARIO_PARSE_FAIL);
		}

		DatabaseHelper databaseHelper = new DatabaseHelper();

		if(usuario.getId() > 0){
			databaseHelper.update(usuario);
		}else{
			Usuario usuarioBanco = new Usuario();
			databaseHelper.query(usuarioBanco, "email = '" + usuario.getEmail() +"'");

			if((usuarioBanco != null) && (usuarioBanco.getId()> 0)){
				return UsuarioErrors.getError(UsuarioErrors.USUARIO_ALREADY_REGISTERED);
			}

			databaseHelper.insert(usuario);
			databaseHelper.query(usuario, "email = '" + usuario.getEmail() +"'");
		}

		return this.parseResponse(usuario);
	}
}
