package br.com.system.modules.usuarios;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
	"facebook",
	"nome",
	"email",
	"nomecompleto"
})
public class Usuario {

	@JsonProperty("id")
	private int id;
	@JsonProperty("facebook")
	private String facebook;
	@JsonProperty("nome")
	private String nome;
	@JsonProperty("email")
	private String email;
	@JsonProperty("nomecompleto")
	private String nomecompleto;

	@JsonProperty("id")
	public int getId() {
		return this.id;
	}

	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("facebook")
	public String getFacebook() {
		return this.facebook;
	}

	@JsonProperty("facebook")
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	@JsonProperty("nome")
	public String getNome() {
		return this.nome;
	}

	@JsonProperty("nome")
	public void setNome(String nome) {
		this.nome = nome;
	}

	@JsonProperty("email")
	public String getEmail() {
		return this.email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("nomecompleto")
	public String getNomecompleto() {
		return this.nomecompleto;
	}

	@JsonProperty("nomecompleto")
	public void setNomecompleto(String nomecompleto) {
		this.nomecompleto = nomecompleto;
	}
}