package br.com.system.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import sun.misc.BASE64Decoder;
import br.com.system.Conf;
import br.com.system.errors.Errors;
import br.com.system.helpers.LogHelper;
import br.com.system.helpers.RequestsHelper;

@WebServlet(name = "UploadsServlet", urlPatterns = { "/uploads", "/uploadsservlet" })
public class UploadsServlet extends HttpServlet {

	private static final long	serialVersionUID			= -25028166381932804L;

	/*
	 * M�todos
	 */
	public static final String	SALVAR_COMPROVANTE_DESPESA			= "salvarComprovanteDespesa";

	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		new RequestsHelper().buildResponse(resp, "Solicita��es GET n�o permitidas.");
	}

	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
		FileItem fileItem = null;
		String dataImage = null;
		List<FileItem> fileItems = null;
		Iterator<FileItem> i = null;
		DiskFileItemFactory factory = null;
		ServletFileUpload upload = null;

		factory = new DiskFileItemFactory();
		factory.setRepository(new File(Conf.TEMP_PATH));

		upload = new ServletFileUpload(factory);

		try{
			fileItems = upload.parseRequest(req);
			i = fileItems.iterator();

			while ( i.hasNext () ){
				FileItem fi = i.next();

				if ( fi.isFormField () ){ //Base64
					String data = fi.getString();
					if((data != null) && (data.indexOf(",") > -1)){
						String typeData = data.substring(0, data.indexOf(","));
						if((typeData != null) && typeData.contains("image") && typeData.contains("base64")){
							dataImage = data;
						}
					}
				}else{ //FILE
					fileItem = fi;
				}
			}

			String result = null;

			if(fileItem != null){ //FILE
				result = this.saveUploadFileItem(fileItem);

			}else if(dataImage != null){ //Base64
				result = this.saveUploadBase64(dataImage);
			}

			if(result == null){
				result = Errors.getError(Errors.UPLOAD_FAIL);
			}

			new RequestsHelper().buildResponse(resp, result);

		}catch(Exception e){
			LogHelper.error(e);
		}
	}

	private String saveUploadBase64(String data){
		if(data == null){
			return null;
		}

		try{
			String fileExtension = data.substring(data.indexOf("/") + 1, data.indexOf(";"));
			String dataImage = data.substring(data.indexOf(",") + 1);
			String fileName = UUID.randomUUID().toString() + "." + fileExtension;
			String path = Conf.TEMP_PATH;
			File file = new File(path + fileName);

			FileOutputStream output = new FileOutputStream(file);
			output.write(new BASE64Decoder().decodeBuffer(dataImage));
			output.flush();
			output.close();

			return fileName;
		}catch(Exception e){
			LogHelper.error(e);
			return null;
		}
	}

	private String saveUploadFileItem(FileItem fileItem){
		if(fileItem == null){
			return null;
		}

		try{
			String[] name = fileItem.getName().split("\\.");
			String fileName = UUID.randomUUID().toString() + "." + name[name.length - 1];
			String path = Conf.TEMP_PATH;

			File file = new File(path + fileName) ;
			fileItem.write(file);

			return fileName;
		}catch(Exception e){
			LogHelper.error(e);
			return null;
		}
	}
}
