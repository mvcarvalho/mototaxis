package br.com.system.helpers;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import br.com.system.Conf;

public class EmailSendHelper {
	
	public static void sendEmailText(final String receiver, final String receivername, final String subject, final String message) throws Exception {
		try {
			Email email = new SimpleEmail();
			
			email.setHostName(Conf.EMAIL_SMTP_HOST_NAME);
			email.setSmtpPort(Conf.EMAIL_SMTP_PORT);
			email.setSslSmtpPort(Conf.EMAIL_SMTP_SSL_PORT);
			email.setSSLOnConnect(true);
			
			email.setAuthenticator(new DefaultAuthenticator(Conf.EMAIL_SMTP_SENDER_MAIL, Conf.EMAIL_SMTP_SENDER_PASSWORD));
			email.setFrom(Conf.EMAIL_SMTP_SENDER_MAIL, Conf.EMAIL_SMTP_SENDER_NAME);
			
			email.setSubject(subject);
			email.setMsg(message);
			email.addTo(receiver, receivername);
			
			email.send();
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static void sendEmailHtml(final String receiver, final String receivername, final String subject, final String message) throws EmailException {
		try {
			HtmlEmail email = new HtmlEmail();
			
			email.setHostName(Conf.EMAIL_SMTP_HOST_NAME);
			email.setSmtpPort(Conf.EMAIL_SMTP_PORT);
			email.setSslSmtpPort(Conf.EMAIL_SMTP_SSL_PORT);
			email.setSSLOnConnect(true);
			
			email.setAuthenticator(new DefaultAuthenticator(Conf.EMAIL_SMTP_SENDER_MAIL, Conf.EMAIL_SMTP_SENDER_PASSWORD));
			email.setFrom(Conf.EMAIL_SMTP_SENDER_MAIL, Conf.EMAIL_SMTP_SENDER_NAME);
			
			email.setSubject(subject);
			email.setHtmlMsg(message);
			email.addTo(receiver, receivername);
			
			email.send();
		} catch (EmailException e) {
			throw e;
		}
	}
}
