package br.com.system.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CurrencyHelper {

	public double converter(final String currency, final String currencyToConvert, final Timestamp date) {

		double value = -1;

		try {

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String link = "http://currencies.apps.grandtrunk.net/getrate/" + dateFormat.format(date) + "/" + currency.trim() + "/" + currencyToConvert.trim();
			URL url = new URL(link);

			InputStreamReader inputReader = new InputStreamReader(url.openStream());
			BufferedReader bufferedReader = new BufferedReader(inputReader);

			String row = "";
			while ((row = bufferedReader.readLine()) != null) {
				value = Double.parseDouble(row);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return value;

	}
}