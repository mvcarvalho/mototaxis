package br.com.system.helpers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.sql.Time;

public class LogHelper {
	
	public static String getStackTraceString(Throwable tr) {
		if (tr == null) {
			return "";
		}
		
		Throwable t = tr;
		while (t != null) {
			if (t instanceof UnknownHostException) {
				return "";
			}
			t = t.getCause();
		}
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		tr.printStackTrace(pw);
		pw.flush();
		return sw.toString();
	}
	
	public static void error(String msg){
		System.out.println("ERRO:\n" + new Time(System.currentTimeMillis()) + "\n" + msg);
	}
	
	public static void error(Exception e){
		LogHelper.error(LogHelper.getStackTraceString(e));
	}
	
	public static void debug(String msg){
		System.out.println("DEBUG:\n" + new Time(System.currentTimeMillis()) + "\n" + msg);
	}
	
	public static void info(String msg){
		System.out.println("INFO:\n" + new Time(System.currentTimeMillis()) + "\n" + msg);
	}
}
