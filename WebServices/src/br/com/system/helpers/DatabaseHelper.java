package br.com.system.helpers;

import java.util.ArrayList;

import br.com.system.persistance.Database;

public class DatabaseHelper {
	
	public void insert(Object object){
		try(Database database = new Database()){
			database.getDatabase().save(object);
		} catch (Exception e) {
			LogHelper.error(e);
		}
	}
	
	public void update(Object object){
		try(Database database = new Database()){
			database.getDatabase().modify(object);
		} catch (Exception e) {
			LogHelper.error(e);
		}
	}
	
	public void delete(Object object){
		try(Database database = new Database()){
			database.getDatabase().delete(object);
		} catch (Exception e) {
			LogHelper.error(e);
		}
	}
	
	public void query(Object object, String sql){
		try(Database database = new Database()){
			database.getDatabase().obtainWhere(object, sql);
		} catch (Exception e) {
			LogHelper.error(e);
		}
	}
	
	public ArrayList<?> queryAll(Object object, String sql){
		try(Database database = new Database()){
			return database.getDatabase().obtainAll(object, sql);
		} catch (Exception e) {
			LogHelper.error(e);
			return null;
		}
	}
}
