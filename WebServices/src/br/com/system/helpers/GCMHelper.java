package br.com.system.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.system.Conf;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;

public class GCMHelper {
	
	public static String SendGCMNotification(final String mensagem, final String deviceId) throws Exception {
		
		try {
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(deviceId);
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("message", mensagem);
			
			JSONObject jsonObjectRoot = new JSONObject();
			jsonObjectRoot.put("registration_ids", jsonArray);
			jsonObjectRoot.put("data", jsonObject);
			
			String postData = jsonObjectRoot.toString();
			
			URL url = new URL("https://android.googleapis.com/gcm/send");
			HttpURLConnection con = GCMHelper.openPostConnection(url);
			
			OutputStream os = con.getOutputStream();
			os.write(postData.getBytes("UTF-8"));
			os.flush();
			os.close();
			
			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return "-1";
			} else {
				return GCMHelper.getResponse(con);
			}
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static HttpURLConnection openPostConnection(URL url) throws Exception {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json; charset=utf8");
		connection.addRequestProperty("Authorization", "key=" + Conf.GOOGLE_CGM_APP_ID);
		
		return connection;
	}
	
	private static String getResponse(HttpURLConnection connection) throws Exception {
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		StringBuilder stringBuilder = new StringBuilder();
		while ((line = rd.readLine()) != null) {
			stringBuilder.append(line);
		}
		rd.close();
		return stringBuilder.toString();
	}
	
}
