package br.com.system.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLTimeoutException;

import br.com.system.Conf;

public class DatabaseConnectionHelper {
	
	public Connection getConnection() throws SQLTimeoutException {
		Connection con = null;
		int tries = 0;
		
		while ((con == null) && (tries < 7)) {
			tries++;
			try {
				con = DriverManager.getConnection(Conf.DATABASE_CONNECTION_STRING);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				con = null;
			}
		}
		
		if (con == null) {
			throw new SQLTimeoutException();
		}
		
		return con;
	}
	
	public void closeConnection(final Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
