package br.com.system.helpers;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import br.com.system.Conf;

public class EmailReadHelper {

	private Session session = null;
	private Store store = null;
	private Folder folder;

	public void connect() throws NoSuchProviderException, MessagingException, IOException {

		Properties props = new Properties();
		props.put("mail.store.protocol", Conf.EMAIL_POP_PROTOCOL);

		try {
			this.session = Session.getDefaultInstance(props);
			this.store = this.session.getStore();
			this.store.connect(Conf.EMAIL_POP_HOST_NAME, Conf.EMAIL_POP_USERNAME, Conf.EMAIL_POP_PASSWORD);
		} catch (NoSuchProviderException e) {
			LogHelper.error(e);
			this.closeConnection();
		} catch (MessagingException e) {
			LogHelper.error(e);
			this.closeConnection();
		} catch (Exception e) {
			LogHelper.error(e);
			this.closeConnection();
		}
	}

	public void closeConnection() {
		try {
			if (this.folder != null) {
				this.folder.close(true);
			}

			if (this.store != null) {
				this.store.close();
			}

		} catch (MessagingException e) {
			LogHelper.error(e);
		}
	}

	public Message[] getMesagesFolder(String f) throws MessagingException {

		if (this.session == null) {
			try {
				this.connect();
			} catch (Exception e) {
				LogHelper.error(e);
				this.closeConnection();
			}
		}

		Flags seen = new Flags(Flags.Flag.SEEN);
		FlagTerm flagTerm = new FlagTerm(seen, false);

		if (f == null) {
			this.folder = this.store.getDefaultFolder().getFolder(Conf.EMAIL_POP_DEFAULT_FOLDER);
		} else {
			this.folder = this.store.getDefaultFolder().getFolder(f);
		}

		this.folder.open(Folder.READ_WRITE);
		Message[] msgs = this.folder.search(flagTerm);

		return msgs;
	}
}
