package br.com.system.helpers;

import java.io.Serializable;
import java.util.ArrayList;

public class CSVHelper implements Serializable {
	
	public static final String COMMA_SEPARATOR = ",";
	public static final String SEMICOLON_SEPARATOR = ";";
	public static final String TAB_SEPARATOR = "\t";
	
	private static final long serialVersionUID = -897137639783211865L;
	
	private String separator;
	private final Sheet sheet;
	
	public CSVHelper(String separator) {
		this.sheet = new Sheet();
		this.setSeparator(separator);
	}
	
	public CSVHelper(String separator, ArrayList<Line> lines) {
		this.sheet = new Sheet(lines);
		this.setSeparator(separator);
	}
	
	public void setSeparator(String separator){
		if ((separator != null) && !separator.equals("")) {
			this.separator = separator;
		} else {
			this.separator = CSVHelper.SEMICOLON_SEPARATOR;
		}
	}
	
	public Sheet getSheet(){
		return this.sheet;
	}
	
	public Line createLine(){
		Line l = new Line();
		this.sheet.addLine(l);
		return l;
	}
	
	public String getCsvText() {
		if (this.sheet != null) {
			StringBuilder strBuilder = new StringBuilder();
			for (Line line : this.sheet.getLines()) {
				strBuilder.append(line.getLineString(this.separator) + "\r\n");
			}
			return strBuilder.toString();
		}
		return null;
	}
	
	public class Sheet {
		private ArrayList<Line> lines;
		
		public Sheet() {
			this.lines = new ArrayList<Line>();
		}
		
		public Sheet(ArrayList<Line> lines) {
			this.lines = lines;
		}
		
		public ArrayList<Line> getLines() {
			return this.lines;
		}
		
		public void setLines(ArrayList<Line> lines) {
			this.lines = lines;
		}
		
		public void addLine(Line line) {
			if (this.lines == null) {
				this.lines = new ArrayList<Line>();
			}
			this.lines.add(line);
		}
	}
	
	public class Line{
		private ArrayList<String> lineData;
		
		public Line(ArrayList<String> data) {
			this.lineData = data;
		}
		
		public Line() {
			this.lineData = new ArrayList<String>();
		}
		
		public ArrayList<String> getLineData() {
			return this.lineData;
		}
		
		public void setLineData(ArrayList<String> data) {
			this.lineData = data;
		}
		
		public void addData(Object data) {
			if (this.lineData == null) {
				this.lineData = new ArrayList<String>();
			}
			this.lineData.add(String.valueOf(data));
		}
		
		public String getLineString(String separator){
			StringBuilder strBuilder = new StringBuilder();
			
			for (int x= 0; x < this.lineData.size(); x++) {
				if((x+1) == this.lineData.size()){
					strBuilder.append("\"" + this.lineData.get(x) + "\"");
				}else{
					strBuilder.append("\"" + this.lineData.get(x) + "\"" + separator);
				}
			}
			
			return strBuilder.toString();
		}
	}
}
