package br.com.system.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public class RequestsHelper {

	public String readRequest(final HttpServletRequest request) {
		try {
			InputStream body = request.getInputStream();
			StringWriter writer = new StringWriter();
			IOUtils.copy(body, writer, "utf-8");
			return writer.toString();
		} catch (Exception e) {
			return null;
		}
	}

	public void buildResponse(HttpServletResponse servlet, String response) throws IOException {
		servlet.setContentType("text/plain; charset=UTF-8");
		servlet.addHeader("Access-Control-Allow-Origin", "*");
		servlet.addHeader("Access-Control-Allow-Methods", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
		servlet.addHeader("Access-Control-Allow-Headers", "GET,POST");
		PrintWriter writer = servlet.getWriter();
		writer.println(response);
	}
}
