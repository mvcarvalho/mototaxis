package br.com.system.helpers;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import br.com.system.Conf;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AmazonS3Helper {

	private AmazonS3 openS3Client(){
		AmazonS3 s3 = null;
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider().getCredentials();
			s3 = new AmazonS3Client(credentials);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s3;
	}

	public String uploadFile(String bucket, byte[] bytes, String ext, boolean publicFile){
		try {
			File file = new File(Conf.TEMP_PATH + "." + ext);
			FileUtils.writeByteArrayToFile(file, bytes);
			return this.uploadFile(bucket, file, publicFile);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String uploadFile(String bucket, String filePath, boolean publicFile){
		File file = new File(filePath);
		return this.uploadFile(bucket, file, publicFile);
	}

	public String uploadFile(String bucket, File file, boolean publicFile){
		if((bucket == null) || (file == null)){
			return null;
		}
		try{
			String key = UUID.randomUUID().toString() + "-" + System.currentTimeMillis() + "." + FilenameUtils.getExtension(file.getPath());
			AmazonS3 s3 = this.openS3Client();

			if(publicFile){
				s3.putObject(new PutObjectRequest(bucket, key, file).withCannedAcl(CannedAccessControlList.PublicRead));
			}else{
				s3.putObject(new PutObjectRequest(bucket, key, file));
			}

			return key;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public byte[] downloadFile(String bucket, String key){
		if((bucket == null) || (key == null)){
			return null;
		}
		try{
			AmazonS3 s3 = this.openS3Client();
			S3Object object = s3.getObject(new GetObjectRequest(bucket, key));
			return IOUtils.toByteArray(object.getObjectContent());
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public boolean deleteFile(String bucket, String key){
		if((bucket == null) || (key == null)){
			return false;
		}
		try{
			AmazonS3 s3 = this.openS3Client();
			s3.deleteObject(bucket, key);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
