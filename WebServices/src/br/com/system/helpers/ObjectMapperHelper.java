package br.com.system.helpers;

import java.util.Collection;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

/**
 * Created by Mateus on 01/07/2015.
 */
public class ObjectMapperHelper {

	private ObjectMapper mapper;

	public ObjectMapper getMapper(){
		if(this.mapper == null){
			this.mapper = this.getNewObjectMapper();
		}
		return this.mapper;
	}

	@SuppressWarnings("rawtypes")
	public CollectionType getCollectionType(Class<? extends Collection> collectionClass, Class<?> elementClass){
		if(this.mapper == null){
			this.mapper = this.getNewObjectMapper();
		}
		return this.mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass);
	}

	private ObjectMapper getNewObjectMapper(){
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		return mapper;
	}
}