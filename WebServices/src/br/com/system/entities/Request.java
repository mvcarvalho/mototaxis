package br.com.system.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"method",
	"params",
	"date"
})
public class Request {

	@JsonProperty("method")
	private String method;
	@JsonProperty("params")
	private String params;
	@JsonProperty("date")
	private long date;

	/**
	 * 
	 * @return
	 * The method
	 */
	@JsonProperty("method")
	public String getMethod() {
		return this.method;
	}

	/**
	 * 
	 * @param method
	 * The method
	 */
	@JsonProperty("method")
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 
	 * @return
	 * The params
	 */
	@JsonProperty("params")
	public String getParams() {
		return this.params;
	}

	/**
	 * 
	 * @param params
	 * The params
	 */
	@JsonProperty("params")
	public void setParams(String params) {
		this.params = params;
	}

	/**
	 * 
	 * @return
	 * The date
	 */
	@JsonProperty("date")
	public long getDate() {
		return this.date;
	}

	/**
	 * 
	 * @param date
	 * The date
	 */
	@JsonProperty("date")
	public void setDate(long date) {
		this.date = date;
	}
}