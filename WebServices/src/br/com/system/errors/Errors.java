package br.com.system.errors;

public class Errors {

	public static final int ERROR_UNKNOW = 10001;
	public static final int INVALID_PARAMS = 10002;
	public static final int ENCRIPTOR_ERROR = 10003;
	public static final int DECRIPT_ERROR = 10004;
	public static final int REQUEST_PARSE_FAIL = 10005;
	public static final int METHOD_NOT_FOUND = 10006;
	public static final int ENCRIPT_ERROR = 10007;
	public static final int RESPONSE_PARSE_FAIL = 10008;
	public static final int UPLOAD_FAIL = 10009;

	public static String getError(int error){
		switch (error) {
		case ERROR_UNKNOW:
			return "{\"error\":\"Erro desconhecido.\", \"errorcode\":\"10001\"}";

		case INVALID_PARAMS:
			return "{\"error\":\"Par�metros da solicita��o inv�lidos.\", \"errorcode\":\"10002\"}";

		case ENCRIPTOR_ERROR:
			return "{\"error\":\"Falha ao iniciar criptografia.\", \"errorcode\":\"10003\"}";

		case DECRIPT_ERROR:
			return "{\"error\":\"Falha ao descriptografar par�metros da solicita��o.\", \"errorcode\":\"10004\"}";

		case REQUEST_PARSE_FAIL:
			return "{\"error\":\"Falha ao mapear par�metros da requisi��o.\", \"errorcode\":\"10005\"}";

		case METHOD_NOT_FOUND:
			return "{\"error\":\"O m�todo solicitado n�o foi encontrado.\", \"errorcode\":\"10006\"}";

		case ENCRIPT_ERROR:
			return "{\"error\":\"Falha ao criptografar resposta.\", \"errorcode\":\"10007\"}";

		case RESPONSE_PARSE_FAIL:
			return "{\"error\":\"Falha ao mapear resposta para texto.\", \"errorcode\":\"10008\"}";

		case UPLOAD_FAIL:
			return "{\"error\":\"Falha ao realizar upload.\", \"errorcode\":\"10009\"}";

		default:
			return "{\"error\":\"Erro desconhecido.\", \"errorcode\":\"00000\"}";
		}
	}
}
