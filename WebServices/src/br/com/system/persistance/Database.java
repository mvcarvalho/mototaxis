package br.com.system.persistance;

import br.com.system.Conf;
import cat.quickdb.db.AdminBase;

public class Database implements AutoCloseable{
	
	private AdminBase instance;
	
	public Database(){
		this.openDatabase();
	}
	
	public AdminBase getDatabase(){
		return this.instance;
	}
	
	private void openDatabase(){
		if(this.instance == null){
			this.instance = AdminBase.initialize(AdminBase.DATABASE.MYSQL, Conf.DATABASE_HOST, Conf.DATABASE_PORT, Conf.DATABASE_NAME, Conf.DATABASE_USER, Conf.DATABASE_PASSWORD);
		}
	}
	
	private void closeDatabase(){
		if(this.instance != null){
			this.instance.close();
			this.instance = null;
		}
	}
	
	@Override
	public void close() throws Exception {
		this.closeDatabase();
	}
}
