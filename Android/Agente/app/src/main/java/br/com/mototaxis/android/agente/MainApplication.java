package br.com.mototaxis.android.agente;

import android.app.Application;

import br.com.mototaxis.android.agente.helpers.SnappyDBHelper;
import br.com.mototaxis.android.agente.modules.agente.Agente;
import br.com.mototaxis.android.agente.modules.solicitacao.Solicitacao;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class MainApplication extends Application {

    private static MainApplication instance;

    public static MainApplication getInstance(){
        return instance;
    }

    private Agente agente;
    private Solicitacao solicitacaoAtiva;

    @Override
    public void onCreate() {
        super.onCreate();

        agente = (Agente) SnappyDBHelper.getInstance().getObject(Config.SNAPPY_KEY_AGENTE, Agente.class, this);
        solicitacaoAtiva = (Solicitacao) SnappyDBHelper.getInstance().getObject(Config.SNAPPY_KEY_SOLICITACAO, Solicitacao.class, this);

        if(agente == null){
            agente = new Agente();
        }

        if(solicitacaoAtiva == null){
            solicitacaoAtiva = new Solicitacao();
        }

        instance = this;
    }

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
        SnappyDBHelper.getInstance().storeObject(Config.SNAPPY_KEY_AGENTE, this.agente, this);
    }

    public Solicitacao getSolicitacaoAtiva() {
        return solicitacaoAtiva;
    }

    public void setSolicitacaoAtiva(Solicitacao solicitacaoAtiva) {
        this.solicitacaoAtiva = solicitacaoAtiva;
        SnappyDBHelper.getInstance().storeObject(Config.SNAPPY_KEY_SOLICITACAO, this.solicitacaoAtiva, this);
    }
}
