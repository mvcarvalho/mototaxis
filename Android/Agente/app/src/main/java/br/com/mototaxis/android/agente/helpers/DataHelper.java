package br.com.mototaxis.android.agente.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.mototaxis.android.agente.Config;

public class DataHelper {

    private SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(Config.SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor(Context context) {
        return context.getSharedPreferences(Config.SHARED_PREFERENCES, Context.MODE_PRIVATE).edit();
    }

    /**
     * Email digitado no formulário de login. Evita o usuário digitar sempre que sair do APP.
     *
     * @param value   String
     * @param context
     */
    public void setGcmToken(String value, Context context) {
        getEditor(context).putString(Config.SP_GCM_TOKEN, value).commit();
    }

    public String getGcmToken(Context context) {
        return getSharedPreferences(context).getString(Config.SP_GCM_TOKEN, "");
    }
}
