package br.com.mototaxis.android.agente.modules.agente;

import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;
import br.com.mototaxis.android.agente.helpers.LogHelper;
import br.com.mototaxis.android.agente.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.agente.helpers.WebServiceHelper;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class AgenteHelper {

    private static String GATEWAY = "agente";

    public void efetuarLogin(Agente agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "efetuarLogin", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void salvarAgente(Agente agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "salvarAgente", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void salvarPosicaoAgente(Agente agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "salvarPosicaoAgente", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }
}
