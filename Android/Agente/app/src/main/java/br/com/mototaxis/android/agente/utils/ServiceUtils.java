package br.com.mototaxis.android.agente.utils;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by Mateus on 22/06/2015.
 */
public class ServiceUtils {

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
