package br.com.mototaxis.android.agente.utils;

import android.animation.Animator;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Mateus on 24/06/2015.
 */
public class ViewsUtils {

    private static final int ANIMATIONS_LENGHT = 200;

    public static void setVisibilityFromAlpha(final View view, final boolean onOff, Activity activity){
        setVisibilityFromAlpha(view, onOff, activity, null);
    }

    public static void setVisibilityFromAlpha(final View view, final boolean onOff, Activity activity, final AnimationEndListener listener){
        if(view != null){
            final Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animation) {}
                @Override public void onAnimationEnd(Animator animation) {if(listener != null){listener.onAnimationEnd();}}
                @Override public void onAnimationCancel(Animator animation) {}
                @Override public void onAnimationRepeat(Animator animation) {}
            };

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(onOff){
                        view.animate().alpha(1).setDuration(ANIMATIONS_LENGHT).setListener(animatorListener);
                    }else{
                        view.animate().alpha(0).setDuration(ANIMATIONS_LENGHT).setListener(animatorListener);
                    }
                }
            });
        }
    }

    public static void setTextViewFromAlpha(final TextView textViewMensagem, final String mensagem, Activity activity){
        setTextViewFromAlpha(textViewMensagem, mensagem, activity, null);
    }

    public static void setTextViewFromAlpha(final TextView textViewMensagem, final String mensagem, Activity activity, final AnimationEndListener listener){
        if(textViewMensagem != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mensagem != null && mensagem.length() > 0){
                        textViewMensagem.setAlpha(0);
                        textViewMensagem.setText(mensagem);
                        textViewMensagem.animate().alpha(1).setDuration(ANIMATIONS_LENGHT).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if(listener != null){listener.onAnimationEnd();}
                            }
                            @Override
                            public void onAnimationCancel(Animator animation) {}
                            @Override
                            public void onAnimationRepeat(Animator animation) {}
                        });
                    }else{
                        textViewMensagem.setAlpha(1);
                        textViewMensagem.animate().alpha(0).setDuration(ANIMATIONS_LENGHT).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                textViewMensagem.setText(mensagem);
                                if(listener != null){listener.onAnimationEnd();}
                            }
                            @Override
                            public void onAnimationCancel(Animator animation) {}
                            @Override
                            public void onAnimationRepeat(Animator animation) {}
                        });
                    }
                }
            });
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public interface AnimationEndListener{
        void onAnimationEnd();
    }
}
