package br.com.mototaxis.android.agente.modules.solicitacao;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import org.json.JSONException;

import java.io.IOException;

import br.com.mototaxis.android.agente.Config;
import br.com.mototaxis.android.agente.MainApplication;
import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;
import br.com.mototaxis.android.agente.helpers.LogHelper;
import br.com.mototaxis.android.agente.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.agente.modules.agente.Agente;
import br.com.mototaxis.android.agente.utils.WebServiceUtils;


/**
 * Created by Mateus Carvalho on 09/07/2015.
 */
public class BuscarSolicitacaoService extends Service {

    private Solicitacao solicitacaoAtiva;

    private Handler mainHandler;
    private Runnable mainRunnable;

    private WebServiceCallback novaCorridaServiceCallback;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initCallbacks();
        createService();
        return START_STICKY;
    }

    private void createService(){
        mainRunnable = new Runnable() {
            @Override
            public void run() {
                mainHandler.removeCallbacks(mainRunnable);
                execSyncTasks();
            }
        };
        mainHandler = new Handler(this.getMainLooper());
        mainHandler.postDelayed(mainRunnable, Config.BUSCAR_SOLICITACAO_INTERVAL);
    }

    private void execSyncTasks(){
        Solicitacao solicitacao = new Solicitacao();
        solicitacao.setStatus("A");
        solicitacao.setLatitude(((MainApplication)getApplication()).getAgente().getLatitude());
        solicitacao.setLongitude(((MainApplication)getApplication()).getAgente().getLongitude());
        solicitacao.setTipo(((MainApplication)getApplication()).getAgente().getTipo());
        new SolicitacaoHelper().recuperarSolicitacaoAberto(solicitacao, novaCorridaServiceCallback);
    }

    private void initCallbacks(){
        novaCorridaServiceCallback = new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    solicitacaoAtiva = ObjectMapperHelper.getMapper().readValue(result, Solicitacao.class);
                    if(solicitacaoAtiva != null && solicitacaoAtiva.getId() > 0){
                        ((MainApplication)getApplication()).setSolicitacaoAtiva(solicitacaoAtiva);
                        sendUpdateBroadcast();
                        return;
                    }
                } catch (IOException e) {
                    LogHelper.error(e);
                }
                mainHandler.postDelayed(mainRunnable, Config.BUSCAR_SOLICITACAO_INTERVAL);
            }
            @Override
            public void onWebServiceError(String error) {
                try {
                    LogHelper.error(WebServiceUtils.getJsonError(error).getString("error"));
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
                mainHandler.postDelayed(mainRunnable, Config.BUSCAR_SOLICITACAO_INTERVAL);
            }
            @Override public void onError() {
                LogHelper.error("Sync Service call error.");
                mainHandler.postDelayed(mainRunnable, Config.BUSCAR_SOLICITACAO_INTERVAL);
            }
        };
    }


    /**
     * Envia um broadcast das informações sensíveis à activity
     */
    private void sendUpdateBroadcast() {
        final Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra(Config.EXTRA_SOLICITACAO, solicitacaoAtiva);
        broadcastIntent.setAction(Config.BROADCAST_SOLICITACAO_RECEIVED);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        this.sendBroadcast(broadcastIntent);
    }
}