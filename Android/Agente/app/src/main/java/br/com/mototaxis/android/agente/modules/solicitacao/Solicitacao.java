package br.com.mototaxis.android.agente.modules.solicitacao;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder({
	"id",
	"guid",
	"idusuario",
	"idagente",
	"data",
	"endereco",
	"comentario",
	"latitude",
	"longitude",
	"status",
	"tipo"
})
public class Solicitacao implements Serializable{

	@JsonProperty("id")
	private int id;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("idusuario")
	private long idusuario;
	@JsonProperty("idagente")
	private long idagente;
	@JsonProperty("data")
	private long data;
	@JsonProperty("endereco")
	private String endereco;
	@JsonProperty("comentario")
	private String comentario;
	@JsonProperty("latitude")
	private double latitude;
	@JsonProperty("longitude")
	private double longitude;
	@JsonProperty("status")
	private String status;
	@JsonProperty("tipo")
	private String tipo;

	/**
	 * 
	 * @return
	 * The id
	 */
	@JsonProperty("id")
	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @param id
	 * The id
	 */
	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 * The guid
	 */
	@JsonProperty("guid")
	public String getGuid() {
		return this.guid;
	}

	/**
	 * 
	 * @param guid
	 * The guid
	 */
	@JsonProperty("guid")
	public void setGuid(String guid) {
		this.guid = guid;
	}

	/**
	 * 
	 * @return
	 * The idusuario
	 */
	@JsonProperty("idusuario")
	public long getIdusuario() {
		return this.idusuario;
	}

	/**
	 * 
	 * @param idusuario
	 * The idusuario
	 */
	@JsonProperty("idusuario")
	public void setIdusuario(long idusuario) {
		this.idusuario = idusuario;
	}

	/**
	 * 
	 * @return
	 * The idagente
	 */
	@JsonProperty("idagente")
	public long getIdagente() {
		return this.idagente;
	}

	/**
	 * 
	 * @param idagente
	 * The idagente
	 */
	@JsonProperty("idagente")
	public void setIdagente(long idagente) {
		this.idagente = idagente;
	}

	/**
	 * 
	 * @return
	 * The data
	 */
	@JsonProperty("data")
	public long getData() {
		return this.data;
	}

	/**
	 * 
	 * @param data
	 * The data
	 */
	@JsonProperty("data")
	public void setData(long data) {
		this.data = data;
	}

	/**
	 * 
	 * @return
	 * The endereco
	 */
	@JsonProperty("endereco")
	public String getEndereco() {
		return this.endereco;
	}

	/**
	 * 
	 * @param endereco
	 * The endereco
	 */
	@JsonProperty("endereco")
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * 
	 * @return
	 * The comentario
	 */
	@JsonProperty("comentario")
	public String getComentario() {
		return this.comentario;
	}

	/**
	 * 
	 * @param comentario
	 * The comentario
	 */
	@JsonProperty("comentario")
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * 
	 * @return
	 * The latitude
	 */
	@JsonProperty("latitude")
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * 
	 * @param latitude
	 * The latitude
	 */
	@JsonProperty("latitude")
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * 
	 * @return
	 * The longitude
	 */
	@JsonProperty("longitude")
	public double getLongitude() {
		return this.longitude;
	}

	/**
	 * 
	 * @param longitude
	 * The longitude
	 */
	@JsonProperty("longitude")
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 
	 * @return
	 * The status
	 */
	@JsonProperty("status")
	public String getStatus() {
		return this.status;
	}

	/**
	 * 
	 * @param status
	 * The status
	 */
	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("tipo")
	public String getTipo() {
		return this.tipo;
	}

	@JsonProperty("tipo")
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}