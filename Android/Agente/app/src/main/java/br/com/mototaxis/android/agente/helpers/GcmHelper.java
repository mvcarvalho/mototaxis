package br.com.mototaxis.android.agente.helpers;

import android.content.Context;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import br.com.mototaxis.android.agente.Config;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class GcmHelper {
    public String registerGcm(Context context) {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        try {
            String token = gcm.register(Config.GOOGLE_APP_ID);
            if(token != null){
                return token;
            }
        }
        catch (IOException e) {
            LogHelper.error(e);
        }
        return null;
    }
}
