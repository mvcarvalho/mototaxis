package br.com.mototaxis.android.agente;

/**
 * Created by Mateus Carvalho on 25/06/2015.
 */
public class Config {
    public static final String WEB_SERVICE_URL = "http://ec2-54-210-131-63.compute-1.amazonaws.com:8080/WebServices/";
    public static final String WEB_SERVICE_CONTENT_TYPE = "application/json; charset=utf-8";

    public static final String SHARED_PREFERENCES = "br.com.mototaxis.android.SHARED_PREFERENCES";
    public static final String SP_GCM_TOKEN = "br.com.mototaxis.android.SP_GCM_TOKEN";

    public static final String GOOGLE_APP_ID = "753717393485";

    public static final long WEB_SERVICE_CONNECT_TIMEOUT = 10000;
    public static final long WEB_SERVICE_READ_TIMEOUT = 30000;
    public static final long WEB_SERVICE_WRITE_TIMEOUT = 30000;

    public static final long UPDATE_INTERVAL = 60000;
    public static final long UPDATE_LOCATION_INTERVAL = 20000;
    public static final long BUSCAR_SOLICITACAO_INTERVAL = 10000;

    public static final String BROADCAST_SYNC_SERVICE_FINISH = "br.com.mototaxis.android.BROADCAST_SYNC_SERVICE_FINISH";
    public static final String BROADCAST_PUSH_RECEIVED = "br.com.mototaxis.android.BROADCAST_PUSH_RECEIVED";
    public static final String BROADCAST_LOCATION_RECEIVED = "br.com.mototaxis.android.BROADCAST_LOCATION_RECEIVED";
    public static final String BROADCAST_SOLICITACAO_RECEIVED = "br.com.mototaxis.android.BROADCAST_SOLICITACAO_RECEIVED";

    public static final String EXTRA_LOCATION = "br.com.mototaxis.android.EXTRA_LOCATION";
    public static final String EXTRA_SOLICITACAO = "br.com.mototaxis.android.EXTRA_SOLICITACAO";

    public static final String DATABASE_NAME = "mototaxis_sqlite_db";
    public static final int DATABASE_VERSION = 1;

    public static final String SNAPPY_DATABASE_NAME = "mototaxis_snappy_db";
    public static final String SNAPPY_KEY_AGENTE = "SNAPPY_KEY_AGENTE";
    public static final String SNAPPY_KEY_SOLICITACAO = "SNAPPY_KEY_SOLICITACAO";

}
