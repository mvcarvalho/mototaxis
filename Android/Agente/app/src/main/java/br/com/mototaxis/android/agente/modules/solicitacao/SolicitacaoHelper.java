package br.com.mototaxis.android.agente.modules.solicitacao;

import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;
import br.com.mototaxis.android.agente.helpers.LogHelper;
import br.com.mototaxis.android.agente.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.agente.helpers.WebServiceHelper;

/**
 * Created by Mateus Carvalho on 29/09/2015.
 */
public class SolicitacaoHelper {

    private static String GATEWAY = "solicitacao";

    public void recuperarSolicitacaoAberto(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "recuperarSolicitacaoAberto", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void aceitarSolicitacao(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "aceitarSolicitacao", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void encerrarSolicitacao(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "encerrarSolicitacao", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }
}
