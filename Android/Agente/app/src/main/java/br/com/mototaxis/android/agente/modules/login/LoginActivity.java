package br.com.mototaxis.android.agente.modules.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;

import br.com.mototaxis.android.agente.MainActivity;
import br.com.mototaxis.android.agente.MainApplication;
import br.com.mototaxis.android.agente.R;
import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;
import br.com.mototaxis.android.agente.helpers.LogHelper;
import br.com.mototaxis.android.agente.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.agente.modules.agente.Agente;
import br.com.mototaxis.android.agente.modules.agente.AgenteHelper;
import br.com.mototaxis.android.agente.modules.map.MapActivity;
import br.com.mototaxis.android.agente.utils.WebServiceUtils;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class LoginActivity extends MainActivity implements View.OnClickListener {

    private TextView textViewMensagem;
    private EditText editTextLogin;
    private EditText editTextSenha;
    private Button buttonLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textViewMensagem = (TextView) findViewById(R.id.text_view_mensagem);
        editTextLogin = (EditText) findViewById(R.id.edit_text_login);
        editTextSenha = (EditText) findViewById(R.id.edit_text_senha);
        buttonLogin = (Button) findViewById(R.id.button_login);

        buttonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_login && editTextLogin != null && editTextSenha != null){

            setMensagem("");

            showProgressBar("Aguarde", "Efetuando Login...", true);

            Agente agente = new Agente();
            agente.setLogin(editTextLogin.getText().toString());
            agente.setSenha(editTextSenha.getText().toString());

            new AgenteHelper().efetuarLogin(agente, new WebServiceCallback() {
                @Override
                public void onSuccess(String result) {
                    try {
                        Agente agente = ObjectMapperHelper.getMapper().readValue(result, Agente.class);
                        if(agente != null && agente.getId() > 0){
                            ((MainApplication)getApplication()).setAgente(agente);
                            startActivity(new Intent(LoginActivity.this, MapActivity.class));
                            finish();
                        }else{
                            setMensagem("Usuário ou senha incorretos.");
                        }

                    } catch (IOException e) {
                        LogHelper.error(e);
                    }
                    cancelProgressBar();
                }

                @Override
                public void onWebServiceError(String error) {
                    LogHelper.error(WebServiceUtils.getJsonError(error).toString());
                    try {
                        setMensagem(WebServiceUtils.getJsonError(error).getString("error"));
                    } catch (JSONException e) {
                        LogHelper.error(e);
                    }
                    cancelProgressBar();
                }

                @Override
                public void onError() {
                    cancelProgressBar();
                    setMensagem("Verifique sua internet e tente novamente.");
                }
            });
        }
    }

    private void setMensagem(final String mensagem){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewMensagem.setText(mensagem);
            }
        });
    }
}
