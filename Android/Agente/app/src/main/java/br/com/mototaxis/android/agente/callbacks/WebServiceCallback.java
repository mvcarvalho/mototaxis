package br.com.mototaxis.android.agente.callbacks;

/**
 * Created by Mateus Carvalho on 07/05/2015.
 */
public interface WebServiceCallback {
    void onSuccess(String result);
    void onWebServiceError(String error);
    void onError();
}
