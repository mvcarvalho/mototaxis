package br.com.mototaxis.android.agente.helpers;

import android.content.Context;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import br.com.mototaxis.android.agente.Config;

/**
 * Created by Mateus on 24/06/2015.
 */
public class SnappyDBHelper {

    private static SnappyDBHelper INSTANCE;
    private DB snappydb;

    private SnappyDBHelper(){}

    synchronized public static SnappyDBHelper getInstance(){
        if(INSTANCE == null){
            INSTANCE = new SnappyDBHelper();
        }
        return INSTANCE;
    }

    synchronized private void openDataBase(Context context){
        try {
            if(snappydb == null){
                snappydb = DBFactory.open(context, Config.SNAPPY_DATABASE_NAME);
            }
        } catch (SnappydbException e) {
            LogHelper.error(e);
        }
    }

    synchronized private void closeDataBase(){
        try {
            if(snappydb != null){
                snappydb.close();
                snappydb = null;
            }
        } catch (SnappydbException e) {
            LogHelper.error(e);
        }
    }

    synchronized public boolean storeObject(String key, Object object, Context context){
        openDataBase(context);

        if(key == null || object == null || snappydb == null){
            return false;
        }

        try {
            snappydb.put(key, object);
            return true;
        } catch (SnappydbException e) {
            LogHelper.error(e);
            return false;
        } finally {
            closeDataBase();
        }
    }

    synchronized public Object getObject(String key, Class<?> classType, Context context){
        openDataBase(context);

        if(key == null || classType == null || snappydb == null){
            return null;
        }

        Object object = null;

        try {
            if(snappydb.exists(key)){
                object = snappydb.getObject(key, classType);
                return object;
            }
        } catch (SnappydbException e) {
            LogHelper.error(e);
        } finally {
            closeDataBase();
        }

        try {
            object = classType.getConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return object;
    }
}
