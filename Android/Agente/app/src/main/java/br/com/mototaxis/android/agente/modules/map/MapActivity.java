package br.com.mototaxis.android.agente.modules.map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.io.IOException;

import br.com.mototaxis.android.agente.Config;
import br.com.mototaxis.android.agente.MainActivity;
import br.com.mototaxis.android.agente.MainApplication;
import br.com.mototaxis.android.agente.R;
import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;
import br.com.mototaxis.android.agente.helpers.LogHelper;
import br.com.mototaxis.android.agente.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.agente.modules.agente.Agente;
import br.com.mototaxis.android.agente.modules.login.LoginActivity;
import br.com.mototaxis.android.agente.modules.solicitacao.BuscarSolicitacaoService;
import br.com.mototaxis.android.agente.modules.solicitacao.Solicitacao;
import br.com.mototaxis.android.agente.modules.solicitacao.SolicitacaoHelper;
import br.com.mototaxis.android.agente.services.LocationService;
import br.com.mototaxis.android.agente.utils.ServiceUtils;
import br.com.mototaxis.android.agente.utils.WebServiceUtils;

/**
 * Created by Mateus Carvalho on 29/09/2015.
 */
public class MapActivity extends MainActivity implements View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker marker;
    private Marker markerSolicitacao;

    private TextView textViewMensagem;
    private Button buttonLogout;
    private Button buttonOcupado;
    private Button buttonEncerrarSolicitacao;

    private Location currentLocation;
    private Solicitacao currentSolicitacao;

    private LocationUpdateReceiver locationUpdateReceiver;
    private IntentFilter locationUpdateFilter;

    private SolicitacaoReceiver solicitacaoReceiver;
    private IntentFilter solicitacaoFilter;

    private boolean ocupado;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ocupado = false;

        locationUpdateReceiver = new LocationUpdateReceiver();
        locationUpdateFilter = new IntentFilter(Config.BROADCAST_LOCATION_RECEIVED);
        locationUpdateFilter.addCategory(Intent.CATEGORY_DEFAULT);

        solicitacaoReceiver = new SolicitacaoReceiver();
        solicitacaoFilter = new IntentFilter(Config.BROADCAST_SOLICITACAO_RECEIVED);
        solicitacaoFilter.addCategory(Intent.CATEGORY_DEFAULT);

        textViewMensagem = (TextView) findViewById(R.id.text_view_mensagem);
        buttonLogout = (Button) findViewById(R.id.button_logout);
        buttonOcupado = (Button) findViewById(R.id.button_ocupado);
        buttonEncerrarSolicitacao = (Button) findViewById(R.id.button_encerrar);

        buttonLogout.setOnClickListener(this);
        buttonOcupado.setOnClickListener(this);
        buttonEncerrarSolicitacao.setOnClickListener(this);

        buttonEncerrarSolicitacao.setVisibility(View.GONE);

        setMensagem("Inicializando...");

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentSolicitacao = getMainApplication().getSolicitacaoAtiva();
        if(currentSolicitacao != null && currentSolicitacao.getId() > 0){
            exibirBotaoEncerrar(true);
            setMensagem("Corrida Aceita.");
        }

        iniciarLocalizacao();
        setUpMapIfNeeded();
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            unregisterReceiver(locationUpdateReceiver);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void iniciarLocalizacao() {
        setMensagem("Buscando localização.");

        if (!ServiceUtils.isServiceRunning(LocationService.class, this)) {
            startService(new Intent(this, LocationService.class));
        }
        try {
            registerReceiver(locationUpdateReceiver, locationUpdateFilter);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void setMensagem(final String mensagem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewMensagem.setText(mensagem);
            }
        });
    }

    private void exibirBotaoEncerrar(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    buttonEncerrarSolicitacao.setVisibility(View.VISIBLE);
                    buttonLogout.setVisibility(View.GONE);
                    buttonOcupado.setVisibility(View.GONE);
                } else {
                    buttonEncerrarSolicitacao.setVisibility(View.GONE);
                    buttonLogout.setVisibility(View.VISIBLE);
                    buttonOcupado.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void iniciarBuscaSolicitacoes() {
        setMensagem("Aguardando nova corrida.");

        if (!ServiceUtils.isServiceRunning(BuscarSolicitacaoService.class, this)) {
            startService(new Intent(this, BuscarSolicitacaoService.class));
        }

        try {
            registerReceiver(solicitacaoReceiver, solicitacaoFilter);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void pararBuscaSolicitacoes() {
        if (ServiceUtils.isServiceRunning(BuscarSolicitacaoService.class, this)) {
            stopService(new Intent(this, BuscarSolicitacaoService.class));
        }

        try {
            unregisterReceiver(solicitacaoReceiver);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void aceitarSolicitacao() {
        setMensagem("Corrida Aceita.");
        showProgressBar("Aguarde", "Confirmando a solicitação...", true);

        currentSolicitacao.setStatus("I");
        currentSolicitacao.setIdagente(getMainApplication().getAgente().getId());
        getMainApplication().setSolicitacaoAtiva(currentSolicitacao);
        new SolicitacaoHelper().aceitarSolicitacao(currentSolicitacao, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    currentSolicitacao = ObjectMapperHelper.getMapper().readValue(result, Solicitacao.class);
                    getMainApplication().setSolicitacaoAtiva(currentSolicitacao);
                } catch (IOException e) {
                    LogHelper.error(e);
                }
                cancelProgressBar();
                exibirBotaoEncerrar(true);
                setUpSolicitacaoLocation();
            }

            @Override
            public void onWebServiceError(String error) {
                cancelProgressBar();
                try {
                    showConfirmDialog(0, "Não foi possível", WebServiceUtils.getJsonError(error).getString("error"), "Ok", null, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            iniciarBuscaSolicitacoes();
                        }
                    }, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
                currentSolicitacao = null;
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao aceitar a corrida. Verifique sua internet.", "Ok", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        iniciarBuscaSolicitacoes();
                    }
                }, null);
                currentSolicitacao = null;
            }
        });
    }

    private void encerrarSolicitacao() {
        setMensagem("Corrida Encerrada.");
        showProgressBar("Aguarde", "Encerrando a corrida...", true);

        currentSolicitacao.setStatus("F");
        currentSolicitacao.setIdagente(getMainApplication().getAgente().getId());
        getMainApplication().setSolicitacaoAtiva(new Solicitacao());
        new SolicitacaoHelper().encerrarSolicitacao(currentSolicitacao, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                currentSolicitacao = null;
                cancelProgressBar();
                exibirBotaoEncerrar(false);
                iniciarBuscaSolicitacoes();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        markerSolicitacao.remove();
                    }
                });
            }

            @Override
            public void onWebServiceError(String error) {
                cancelProgressBar();
                try {
                    showConfirmDialog(0, "Não foi possível", WebServiceUtils.getJsonError(error).getString("error"), "Ok", null, null, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao encerrar a corrida. Verifique sua internet.", "Ok", null, null, null);
            }
        });
    }

    private void logout() {
        if (ServiceUtils.isServiceRunning(LocationService.class, this)) {
            stopService(new Intent(this, LocationService.class));
        }

        getMainApplication().setAgente(new Agente());
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_logout) {
            showConfirmDialog(0, "Sair", "Tem certeza que deseja sair do 88Motos? Você deixará de receber corridas.", "Sair", "Voltar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                        }
                    }, null);
        }

        if (v.getId() == R.id.button_encerrar) {
            showConfirmDialog(0, "Encerrar", "Deseja realmente encerrar esta corrida?", "Encerrar", "Voltar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    encerrarSolicitacao();
                }
            }, null);
        }

        if (v.getId() == R.id.button_ocupado) {

            String msg;
            if(ocupado){
                msg = "Deseja voltar a ficar disponível? Voce voltará a receber corridas.";
            }else{
                msg = "Você não irá receber novas corridas até voltar a ficar disponível. Tem certeza?";
            }

            showConfirmDialog(0, "Ficar ocupado", msg, "Ok", "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(ocupado){
                        ocupado = false;
                        buttonOcupado.setText("Ficar Ocupado");
                    }else{
                        ocupado = true;
                        buttonOcupado.setText("Ficar Disponível");
                    }
                }
            }, null);
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpCurrentLocation();
                setUpSolicitacaoLocation();
            }
        }
    }

    private void setUpCurrentLocation() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentLocation != null) {
                    LatLng newLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    if (marker == null) {
                        marker = mMap.addMarker(new MarkerOptions().position(newLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin)).anchor(0.5f, 0.5f).title("Marker"));
                    } else {
                        marker.setPosition(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                    }
                    if(currentSolicitacao != null && currentSolicitacao.getId() > 0){
                        LatLngBounds bounds = LatLngBounds.builder()
                                .include(new LatLng(currentSolicitacao.getLatitude(), currentSolicitacao.getLongitude()))
                                .include(newLatLng).build();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                    }else{
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, 17));
                    }
                }
            }
        });
    }

    private void setUpSolicitacaoLocation() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentSolicitacao != null && currentSolicitacao.getId() > 0) {
                    LatLng newLatLng = new LatLng(currentSolicitacao.getLatitude(), currentSolicitacao.getLongitude());
                    if (markerSolicitacao == null) {
                        markerSolicitacao = mMap.addMarker(new MarkerOptions().position(newLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin_yellow)).anchor(0.5f, 1).title("Marker"));
                    } else {
                        markerSolicitacao.setPosition(new LatLng(currentSolicitacao.getLatitude(), currentSolicitacao.getLongitude()));
                    }
                    LatLngBounds bounds = LatLngBounds.builder()
                            .include(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                            .include(newLatLng).build();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                }
            }
        });
    }

    public class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent data) {
            if (data.hasExtra(Config.EXTRA_LOCATION)) {
                if (currentLocation == null) {
                    iniciarBuscaSolicitacoes();
                }
                currentLocation = (Location) data.getExtras().get(Config.EXTRA_LOCATION);
                setUpCurrentLocation();
                setUpSolicitacaoLocation();
            }
        }
    }

    public class SolicitacaoReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent data) {
            if (data.hasExtra(Config.EXTRA_SOLICITACAO) && !ocupado) {

                pararBuscaSolicitacoes();

                currentSolicitacao = (Solicitacao) data.getExtras().get(Config.EXTRA_SOLICITACAO);
                setMensagem("Nova corrida recebida.");

                showConfirmDialog(0, "Nova Corrida", "Você recebeu uma nova corrida:\n" + currentSolicitacao.getEndereco(), "Aceitar", "Recusar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                aceitarSolicitacao();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                iniciarBuscaSolicitacoes();
                            }
                        }
                );
            }
        }
    }
}
