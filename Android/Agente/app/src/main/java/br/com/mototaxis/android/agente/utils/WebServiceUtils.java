package br.com.mototaxis.android.agente.utils;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.mototaxis.android.agente.helpers.LogHelper;

/**
 * Created by Mateus Carvalho on 29/06/2015.
 */
public class WebServiceUtils {

    public static JSONObject getJsonError(String json){
        try{
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject;
        }catch (Exception e){
            LogHelper.error(e);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("error", "Erro desconhecido.");
            jsonObject.put("errorcode", 00000);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
