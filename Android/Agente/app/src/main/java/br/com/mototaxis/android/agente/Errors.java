package br.com.mototaxis.android.agente;

/**
 * Created by Mateus on 18/06/2015.
 */
public class Errors {

    //Erro na execução.
    public static final String ERROR_WEB_SERVICE_PARAMETROS = "Falha ao executar requisição no Web Service. Parâmetros inválidos. ";
    public static final String ERROR_WEB_SERVICE_INVALID_JSON_REQUEST = "Falha ao executar requisição no Web Service. Falha ao construir JSON com parâmetros. ";
    public static final String ERROR_WEB_SERVICE_TIMEOUT = "Falha ao executar requisição no Web Service. Timeout. ";

    //Erro na resposta
    public static final String ERROR_WEB_SERVICE_RESPONSE_NULL = "Falha ao processar resposta do Web Service. A resposta do servidor foi nula (NULL). ";
    public static final String ERROR_WEB_SERVICE_INVALID_RESPONSE = "Falha ao processar resposta do Web Service. String retornada não está no formato JSON. ";
    public static final String ERROR_WEB_SERVICE_INVALID_JSON_RESPONSE = "Falha ao processar resposta do Web Service. JSON em formato inválido. ";

    //Resposta completa com erro informado pelo servidor.
    public static final String ERROR_WEB_SERVICE_RESPONSE_STATUS = "Resposta do Web Service processada. Retornado status de erro. ";

    //Erro ao ler dados do JSON
    public static final String ERROR_JSON_LER_DADOS = "Não foi possível recuperar os dados do JSON. ";

    //Erro genérico no login
    public static final String ERROR_LOGIN_DESCONHECIDO = "Falha no login. Verifique a internet. ";
}
