package br.com.mototaxis.android.agente.helpers;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.mototaxis.android.agente.Config;
import br.com.mototaxis.android.agente.Errors;
import br.com.mototaxis.android.agente.callbacks.WebServiceCallback;

/**
 * Created by Mateus Carvalho on 07/05/2015.
 */
public class WebServiceHelper {

    public void callWebService(final String gateway, final String method, final String params, final WebServiceCallback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(gateway != null && params != null && method != null){
                    String requestResult = null;

                    try{
                        requestResult = executePostCall(gateway, method, params).body().string();
                    } catch (IOException e) {
                        LogHelper.error(e);
                        notifyError(callback);
                        return;
                    } catch (JSONException e) {
                        LogHelper.error(e);
                        notifyError(callback);
                        return;
                    }

                    if(requestResult == null){
                        LogHelper.error(Errors.ERROR_WEB_SERVICE_RESPONSE_NULL + method);
                        notifyError(callback);
                        return;
                    }

                    if(requestResult.startsWith("{")){
                        try {
                            JSONObject json = new JSONObject(requestResult);

                            if(json.has("error") || json.has("errorcode")){
                                String erro = Errors.ERROR_WEB_SERVICE_RESPONSE_STATUS + method;
                                if(json.has("error")){
                                    erro += " - " + json.getString("error");
                                }
                                if(json.has("errorcode")){
                                    erro += " - " + json.getString("errorcode");
                                }
                                LogHelper.error(erro);
                                notifyWebServiceError(callback, json.toString());
                                return;
                            }
                            notifySuccess(callback, json.toString());
                            LogHelper.debug(requestResult);
                            return;
                        } catch (Exception e) {
                            LogHelper.debug(e.getMessage());
                        }
                    }

                    if(requestResult.startsWith("[")){
                        try {
                            JSONArray json = new JSONArray(requestResult);
                            notifySuccess(callback, json.toString());
                            LogHelper.debug(requestResult);
                            return;
                        } catch (Exception e) {
                            LogHelper.debug(e.getMessage());
                        }
                    }

                    LogHelper.error(Errors.ERROR_WEB_SERVICE_INVALID_RESPONSE + method + " - " + requestResult);
                    notifyError(callback);
                }else{
                    LogHelper.error(Errors.ERROR_WEB_SERVICE_PARAMETROS + method);
                    notifyError(callback);
                }
            }
        }).start();
    }

    private String getFormattedParams(String method, String params) throws JSONException {
        //JSONObject parametros = new JSONObject(params);
        JSONObject json = new JSONObject();
        json.put("method", method);
        json.put("params", params);
        json.put("date", System.currentTimeMillis());
        return json.toString();
    }

    private Response executePostCall(String gateway, String method, String params) throws IOException, JSONException {
        //Define o Tipo de Conteúdo da chamada.
        MediaType JSON = MediaType.parse(Config.WEB_SERVICE_CONTENT_TYPE);

        //Define o conteúdo da chamada.
        RequestBody body = RequestBody.create(JSON, getFormattedParams(method, params));

        //Constrói a requisição para a chamada.
        Request request = new Request.Builder().url(Config.WEB_SERVICE_URL + gateway).post(body).build();

        //Cria OkHTTPClient e define os timeouts.
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(Config.WEB_SERVICE_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        client.setReadTimeout(Config.WEB_SERVICE_READ_TIMEOUT, TimeUnit.MILLISECONDS);
        client.setWriteTimeout(Config.WEB_SERVICE_WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
        return client.newCall(request).execute();
    }

    private void notifyWebServiceError(WebServiceCallback callback, String error){
        if(callback != null){
            callback.onWebServiceError(error);
        }
    }

    private void notifyError(WebServiceCallback callback){
        if(callback != null){
            callback.onError();
        }
    }

    private void notifySuccess(WebServiceCallback callback, String result){
        if(callback != null){
            callback.onSuccess(result);
        }
    }
}
