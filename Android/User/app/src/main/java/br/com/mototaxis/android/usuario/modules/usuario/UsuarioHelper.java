package br.com.mototaxis.android.usuario.modules.usuario;

import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.usuario.helpers.WebServiceHelper;
import br.com.mototaxis.android.usuario.modules.usuario.Usuario;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class UsuarioHelper {

    private static String GATEWAY = "usuario";

    public void recuperarUsuario(Usuario usuario, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(usuario);
            new WebServiceHelper().callWebService(GATEWAY, "recuperarUsuario", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void salvarUsuario(Usuario usuario, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(usuario);
            new WebServiceHelper().callWebService(GATEWAY, "salvarUsuario", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }
}
