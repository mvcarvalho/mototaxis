package br.com.mototaxis.android.usuario.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import br.com.mototaxis.android.usuario.Config;
import br.com.mototaxis.android.usuario.MainActivity;
import br.com.mototaxis.android.usuario.utils.NotificationUtils;

/**
 * Created by Mateus Carvalho on 05/08/2015.
 */
public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            final String messageType = gcm.getMessageType(intent);

            if (messageType != null) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    String msg = intent.getExtras().getString("message");
                }
            }
        }
    }

    private void exibirNotificacao(Context context, String titulo, String mensagem){
        Intent intentResult = new Intent(context, MainActivity.class);
        NotificationUtils.openNotification(context, intentResult, 0, "", titulo, mensagem);
        sendUpdateBroadcast(context);
    }

    private void sendUpdateBroadcast(Context context) {
        final Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Config.BROADCAST_PUSH_RECEIVED);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        context.sendBroadcast(broadcastIntent);
    }
}
