package br.com.mototaxis.android.usuario.helpers;

public class LogHelper {

    private static final String TAG = "br.com.mototaxis";

    public static void debug(String message){
        android.util.Log.d(TAG, message);
    }

    public static void error(String message){
        android.util.Log.e(TAG, message);
    }

    public static void error(Exception e){
        android.util.Log.e(TAG, android.util.Log.getStackTraceString(e));
    }

    public static void info(String message){
        android.util.Log.i(TAG, message);
    }

}
