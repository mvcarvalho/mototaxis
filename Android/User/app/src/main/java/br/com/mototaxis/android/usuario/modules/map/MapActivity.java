package br.com.mototaxis.android.usuario.modules.map;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SyncStatusObserver;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.nfc.tech.NfcBarcode;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import br.com.mototaxis.android.usuario.Config;
import br.com.mototaxis.android.usuario.MainActivity;
import br.com.mototaxis.android.usuario.MainApplication;
import br.com.mototaxis.android.usuario.R;
import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.usuario.modules.agente.Agente;
import br.com.mototaxis.android.usuario.modules.agente.AgenteHelper;
import br.com.mototaxis.android.usuario.modules.solicitacao.Solicitacao;
import br.com.mototaxis.android.usuario.modules.solicitacao.SolicitacaoHelper;
import br.com.mototaxis.android.usuario.modules.usuario.Usuario;
import br.com.mototaxis.android.usuario.modules.usuario.UsuarioHelper;
import br.com.mototaxis.android.usuario.services.LocationService;
import br.com.mototaxis.android.usuario.utils.ServiceUtils;
import br.com.mototaxis.android.usuario.utils.WebServiceUtils;

/**
 * Created by Mateus Carvalho on 29/09/2015.
 */
public class MapActivity extends MainActivity implements View.OnClickListener {

    private AccountManager accountManager;
    private Geocoder geocoder;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker marker;

    private View viewForm;
    private View viewAgente;
    private View viewLogin;
    private TextView textViewTipo;
    private TextView textViewMensagem;
    private TextView textViewAgente;
    private TextView textViewTelefone;
    private TextView textViewMoto;
    private TextView textViewPlacaMoto;
    private EditText editTextEndereco;
    private EditText editTextObservacao;
    private Button buttonSolicitar;
    private Button buttonSolicitarTaxi;
    private Button buttonConfirmar;
    private Button buttonVoltar;
    private Button buttonJaChegou;
    private Button buttonFechar;
    private Button buttonInformacoes;
    private Button buttonCancelarLogin;
    private Button buttonLoginGoogle;

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    private WebServiceCallback webServiceCallbackLogin;
    private WebServiceCallback webServiceCallbackRegister;

    private String tipo;

    private Location currentLocation;
    private Solicitacao currentSolicitacao;
    private Agente currentAgente;
    private Usuario usuario;

    private LocationUpdateReceiver locationUpdateReceiver;
    private IntentFilter locationUpdateFilter;

    private Handler mainHandler;
    private Runnable mainRunnable;

    private Thread getAddressThread;

    private int count;
    private boolean isRequestLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        accountManager = AccountManager.get(this);
        geocoder = new Geocoder(this, Locale.getDefault());

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_map);

        initFacebookLogin();

        locationUpdateReceiver = new LocationUpdateReceiver();
        locationUpdateFilter = new IntentFilter(Config.BROADCAST_LOCATION_RECEIVED);
        locationUpdateFilter.addCategory(Intent.CATEGORY_DEFAULT);

        viewForm = findViewById(R.id.layout_form);
        viewAgente = findViewById(R.id.layout_agente);
        viewLogin = findViewById(R.id.layout_login);
        textViewTipo = (TextView) findViewById(R.id.text_view_tipo);
        textViewMensagem = (TextView) findViewById(R.id.text_view_mensagem);
        textViewAgente = (TextView) findViewById(R.id.text_view_agente);
        textViewTelefone = (TextView) findViewById(R.id.text_view_telefone);
        textViewMoto = (TextView) findViewById(R.id.text_view_moto);
        textViewPlacaMoto = (TextView) findViewById(R.id.text_view_placa_moto);
        editTextEndereco = (EditText) findViewById(R.id.edit_text_endereco);
        editTextObservacao = (EditText) findViewById(R.id.edit_text_observacao);
        buttonSolicitar = (Button) findViewById(R.id.button_nova_solicitacao);
        buttonSolicitarTaxi = (Button) findViewById(R.id.button_nova_solicitacao_taxi);
        buttonConfirmar = (Button) findViewById(R.id.button_confirmar);
        buttonVoltar = (Button) findViewById(R.id.button_voltar);
        buttonJaChegou = (Button) findViewById(R.id.button_cancelar);
        buttonFechar = (Button) findViewById(R.id.button_fechar);
        buttonInformacoes = (Button) findViewById(R.id.button_informacoes);
        buttonCancelarLogin = (Button) findViewById(R.id.button_cancelar_login);
        buttonLoginGoogle = (Button) findViewById(R.id.login_google_button);

        buttonSolicitar.setOnClickListener(this);
        buttonSolicitarTaxi.setOnClickListener(this);
        buttonConfirmar.setOnClickListener(this);
        buttonJaChegou.setOnClickListener(this);
        buttonVoltar.setOnClickListener(this);
        buttonFechar.setOnClickListener(this);
        buttonInformacoes.setOnClickListener(this);
        buttonCancelarLogin.setOnClickListener(this);
        buttonLoginGoogle.setOnClickListener(this);

        buttonSolicitar.setVisibility(View.GONE);
        buttonSolicitarTaxi.setVisibility(View.GONE);
        buttonJaChegou.setVisibility(View.GONE);
        buttonInformacoes.setVisibility(View.GONE);
        viewForm.setVisibility(View.GONE);
        viewAgente.setVisibility(View.GONE);
        viewLogin.setVisibility(View.GONE);

        setMensagem("Inicializando...");

        initCallbacks();
        iniciarLocalizacao();
        setUpMapIfNeeded();

//        Profile p = Profile.getCurrentProfile();
//        if(p != null && p.getId() != null && !"".equals(p.getId()) && getMainApplication().getUsuario().getId() <= 0){
//            isRequestLogin = false;
//            getFacebookUserDetails(AccessToken.getCurrentAccessToken());
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
        setUpMapIfNeeded();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            unregisterReceiver(locationUpdateReceiver);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void getFacebookUserDetails(AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                usuario = new Usuario();

                try {
                    if (object.has("email")) {
                        usuario.setEmail(object.getString("email"));
                    }
                } catch (Exception e) {
                    LogHelper.error(e);
                }

                try {
                    if (object.has("id")) {
                        usuario.setFacebook(object.getString("id"));
                    }
                } catch (Exception e) {
                    LogHelper.error(e);
                }

                try {
                    if (object.has("first_name")) {
                        usuario.setNome(object.getString("first_name"));
                    }
                } catch (Exception e) {
                    LogHelper.error(e);
                }

                try {
                    if (object.has("name")) {
                        usuario.setNomecompleto(object.getString("name"));
                    }
                } catch (Exception e) {
                    LogHelper.error(e);
                }

                exibirLogin(false);
                showProgressBar("Aguarde", "Efetuando login, aguarde...", true);
                new UsuarioHelper().recuperarUsuario(usuario, webServiceCallbackLogin);
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,name,email,gender,birthday,location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void initFacebookLogin() {
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email"));

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getFacebookUserDetails(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                LogHelper.debug("Cancelado");
            }

            @Override
            public void onError(FacebookException exception) {
                LogHelper.debug("Erro");
            }
        });
    }

    private void initCallbacks(){
        webServiceCallbackLogin = new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    usuario = ObjectMapperHelper.getMapper().readValue(result, Usuario.class);
                    if (usuario != null && usuario.getId() > 0) {
                        getMainApplication().setUsuario(usuario);
                        if(isRequestLogin){
                            exibirFormulario(true);
                        }
                    }else{
                        showConfirmDialog(0, "Falha", "Ocorreu um erro ao realizar login. Verifique a internet e tente novamente.", "Ok", null, null, null);
                    }
                    cancelProgressBar();
                } catch (Exception e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onWebServiceError(String error) {
                try {
                    JSONObject json = WebServiceUtils.getJsonError(error);
                    if(json.getLong("errorcode") == 20004){
                        new UsuarioHelper().salvarUsuario(usuario, webServiceCallbackRegister);
                        return;
                    }

                    cancelProgressBar();
                    showConfirmDialog(0, "Não foi possível", json.getString("error") + " Verifique a internet e tente novamente.", "Ok", null, null, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao realizar login. Verifique a internet e tente novamente.", "Ok", null, null, null);
            }
        };

        webServiceCallbackRegister  = new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    usuario = ObjectMapperHelper.getMapper().readValue(result, Usuario.class);
                    if (usuario != null && usuario.getId() > 0) {
                        getMainApplication().setUsuario(usuario);
                        if(isRequestLogin){
                            exibirFormulario(true);
                        }
                    }else{
                        showConfirmDialog(0, "Falha", "Ocorreu um erro ao realizar login. Verifique a internet e tente novamente.", "Ok", null, null, null);
                    }
                    cancelProgressBar();
                } catch (Exception e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onWebServiceError(String error) {
                try {
                    cancelProgressBar();
                    showConfirmDialog(0, "Não foi possível", WebServiceUtils.getJsonError(error).getString("error") + " Verifique a internet e tente novamente.", "Ok", null, null, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao realizar login. Verifique a internet e tente novamente.", "Ok", null, null, null);
            }
        };
    }

    private void iniciarLocalizacao() {
        setMensagem("Buscando localização.");

        if (!ServiceUtils.isServiceRunning(LocationService.class, this)) {
            startService(new Intent(this, LocationService.class));
        }
        try {
            registerReceiver(locationUpdateReceiver, locationUpdateFilter);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void cancelarLocalizacao() {
        if (ServiceUtils.isServiceRunning(LocationService.class, this)) {
            stopService(new Intent(this, LocationService.class));
        }
        try {
            unregisterReceiver(locationUpdateReceiver);
        } catch (Exception e) {
            LogHelper.error(e);
        }
    }

    private void setMensagem(final String mensagem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mensagem != null && mensagem.length() > 0) {
                    textViewMensagem.setText(mensagem);
                    textViewMensagem.setVisibility(View.VISIBLE);
                } else {
                    textViewMensagem.setText("");
                    textViewMensagem.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirBotaoSolicitar(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    buttonSolicitar.setVisibility(View.VISIBLE);
                    buttonSolicitarTaxi.setVisibility(View.VISIBLE);
                } else {
                    buttonSolicitar.setVisibility(View.GONE);
                    buttonSolicitarTaxi.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirBotaoJaChegou(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    buttonJaChegou.setVisibility(View.VISIBLE);
                } else {
                    buttonJaChegou.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirBotaoInformacoes(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    buttonInformacoes.setVisibility(View.VISIBLE);
                } else {
                    buttonInformacoes.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirFormulario(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    if ("T".equals(tipo)) {
                        textViewTipo.setText("Pedir Táxi");
                    } else if ("M".equals(tipo)) {
                        textViewTipo.setText("Pedir Mototaxi");
                    } else {
                        textViewTipo.setText("");
                    }

                    viewForm.setVisibility(View.VISIBLE);
                } else {
                    viewForm.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirLogin(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    viewLogin.setVisibility(View.VISIBLE);
                } else {
                    viewLogin.setVisibility(View.GONE);
                }
            }
        });
    }

    private void exibirAgente(final boolean exibir) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (exibir) {
                    viewAgente.setVisibility(View.VISIBLE);
                } else {
                    viewAgente.setVisibility(View.GONE);
                }
            }
        });
    }

    private void iniciarNovaSolicitacao() {
        setMensagem("Nova Corrida");
        showProgressBar("Aguarde", "Solicitando a nova corrida...", true);

        currentSolicitacao = new Solicitacao();
        currentSolicitacao.setComentario(editTextObservacao.getText().toString());
        currentSolicitacao.setEndereco(editTextEndereco.getText().toString());
        currentSolicitacao.setLatitude(currentLocation.getLatitude());
        currentSolicitacao.setLongitude(currentLocation.getLongitude());
        currentSolicitacao.setIdagente(0);
        currentSolicitacao.setIdusuario(getMainApplication().getUsuario().getId());
        currentSolicitacao.setStatus("A");
        currentSolicitacao.setTipo(tipo);

        new SolicitacaoHelper().iniciarSolicitacao(currentSolicitacao, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    currentSolicitacao = ObjectMapperHelper.getMapper().readValue(result, Solicitacao.class);
                    if (currentSolicitacao != null && currentSolicitacao.getId() > 0) {
                        ((MainApplication) getApplication()).setSolicitacaoAtiva(currentSolicitacao);
                    }
                    cancelProgressBar();
                    aguardarAceite();
                } catch (Exception e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onWebServiceError(String error) {
                cancelProgressBar();
                try {
                    showConfirmDialog(0, "Não foi possível", WebServiceUtils.getJsonError(error).getString("error"), "Ok", null, null, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao solicitar a corrida. Verifique sua internet e tente novamente.", "Ok", null, null, null);
            }
        });
    }

    private void cancelarSolicitacao() {
        new SolicitacaoHelper().cancelarSolicitacao(currentSolicitacao, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                cancelProgressBar();
                showConfirmDialog(0, "Ops!", "Nenhum mototaxista aceitou a corrida. Tente novamente em alguns instantes.", "Ok", null, null, null);
                exibirBotaoSolicitar(true);
                exibirBotaoJaChegou(false);
                exibirBotaoInformacoes(false);
            }

            @Override
            public void onWebServiceError(String error) {
                cancelProgressBar();
                try {
                    showConfirmDialog(0, "ops!", WebServiceUtils.getJsonError(error).getString("error"), "Ok", null, null, null);
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
                exibirBotaoSolicitar(true);
                exibirBotaoJaChegou(false);
                exibirBotaoInformacoes(false);
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Falha", "Ocorreu um erro ao cancelar a corrida. Verifique sua internet e tente novamente.", "Ok", null, null, null);
                exibirBotaoSolicitar(true);
                exibirBotaoJaChegou(false);
                exibirBotaoInformacoes(false);
            }
        });
    }

    private void aguardarAceite() {
        count = 60;
        setMensagem("Corrida Solicitada");
        showProgressBar("Aguarde", "Epere até algum agente aceitar a corrida (" + count + ")", true);

        mainRunnable = new Runnable() {
            @Override
            public void run() {
                count -= 2;
                updateProgressBar("Aguarde", "Epere até algum agente aceitar a corrida (" + count + ")");
                mainHandler.removeCallbacks(mainRunnable);

                if (count <= 1) {
                    cancelarSolicitacao();
                    return;
                }

                new SolicitacaoHelper().recuperarSolicitacao(currentSolicitacao, new WebServiceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            currentSolicitacao = ObjectMapperHelper.getMapper().readValue(result, Solicitacao.class);
                            if (currentSolicitacao != null && currentSolicitacao.getId() > 0) {
                                ((MainApplication) getApplication()).setSolicitacaoAtiva(currentSolicitacao);
                                if (currentSolicitacao.getStatus() != null && currentSolicitacao.getStatus().equals("I")) {
                                    recuperarAgente();
                                    return;
                                }
                            }
                        } catch (Exception e) {
                            LogHelper.error(e);
                        }
                        mainHandler.postDelayed(mainRunnable, 2000);
                    }

                    @Override
                    public void onWebServiceError(String error) {
                        mainHandler.postDelayed(mainRunnable, 2000);
                    }

                    @Override
                    public void onError() {
                        mainHandler.postDelayed(mainRunnable, 2000);
                    }
                });
            }
        };
        mainHandler = new Handler(this.getMainLooper());
        mainHandler.postDelayed(mainRunnable, 2000);
    }

    private void recuperarAgente() {
        Agente agente = new Agente();
        agente.setId(currentSolicitacao.getIdagente());
        new AgenteHelper().recuperarAgente(agente, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    currentAgente = ObjectMapperHelper.getMapper().readValue(result, Agente.class);
                    if (currentAgente != null && currentAgente.getId() > 0) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textViewAgente.setText(currentAgente.getNome());
                                textViewTelefone.setText(currentAgente.getTelefone1());
                                textViewPlacaMoto.setText(currentAgente.getPlacamoto());
                                textViewMoto.setText(currentAgente.getModelomoto() + " - " + currentAgente.getCormoto());
                            }
                        });

                        cancelProgressBar();
                        showConfirmDialog(0, "Corrida aceita", "Sua corrida foi aceita. O agente " + currentAgente.getNome() + " já está a caminho.", "Ok", null, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                exibirAgente(true);
                            }
                        }, null);
                        setMensagem("Agente a caminho.");
                        exibirFormulario(false);
                        exibirBotaoSolicitar(false);
                        exibirBotaoJaChegou(true);
                        exibirBotaoInformacoes(true);
                        return;
                    }
                } catch (Exception e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onWebServiceError(String error) {
                cancelProgressBar();
                showConfirmDialog(0, "Corrida aceita", "Sua corrida foi aceita. Um agente já está a caminho.", "Ok", null, null, null);
                setMensagem("Agente a caminho.");
                exibirFormulario(false);
                exibirBotaoSolicitar(false);
                exibirBotaoJaChegou(true);
                exibirBotaoInformacoes(false);
            }

            @Override
            public void onError() {
                cancelProgressBar();
                showConfirmDialog(0, "Corrida aceita", "Sua corrida foi aceita. Um agente já está a caminho.", "Ok", null, null, null);
                setMensagem("Agente a caminho.");
                exibirFormulario(false);
                exibirBotaoSolicitar(false);
                exibirBotaoJaChegou(true);
                exibirBotaoInformacoes(false);
            }
        });
    }

    private void confirmarChegada() {
        showConfirmDialog(0, "Já chegou?", "O mototaxista já chegou? É só confirmar ou aguardar mais um pouco. Qualquer coisa, o telefone dele está em informações.", "Já chegou", "Ainda não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exibirBotaoSolicitar(true);
                exibirBotaoJaChegou(false);
                exibirBotaoInformacoes(false);
                iniciarLocalizacao();
            }
        }, null);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_nova_solicitacao) {

            if (getMainApplication().getUsuario().getId() <= 0) {
                isRequestLogin = true;
                exibirLogin(true);
                return;
            }

            tipo = "M";
            exibirFormulario(true);
        }

        if (v.getId() == R.id.button_cancelar_login) {
            exibirLogin(false);
        }

        if (v.getId() == R.id.login_google_button) {
            openSelectGoogleAccountDialog();
        }

        if (v.getId() == R.id.button_nova_solicitacao_taxi) {

            if (getMainApplication().getUsuario().getId() <= 0) {
                isRequestLogin = true;
                exibirLogin(true);
                return;
            }

            tipo = "T";
            exibirFormulario(true);
        }

        if (v.getId() == R.id.button_voltar) {
            exibirFormulario(false);
        }

        if (v.getId() == R.id.button_confirmar) {
            iniciarNovaSolicitacao();
        }

        if (v.getId() == R.id.button_cancelar) {
            confirmarChegada();
        }

        if (v.getId() == R.id.button_fechar) {
            exibirAgente(false);
        }

        if (v.getId() == R.id.button_informacoes) {
            exibirAgente(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        if (currentLocation != null) {
            LatLng newLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            if (marker == null) {
                marker = mMap.addMarker(new MarkerOptions().position(newLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin)));
            } else {
                marker.setPosition(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, 17));
        }
    }

    private void openSelectGoogleAccountDialog() {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        String[] contas = new String[accounts.length];
        for (int i = 0; i < accounts.length; i++) {
            contas[i] = accounts[i].name;
        }

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title("Login").items(contas);
        builder.itemsCallback(new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                exibirLogin(false);
                showProgressBar("Aguarde", "Efetuando login, aguarde...", true);
                usuario = new Usuario();
                usuario.setEmail(text.toString());
                new UsuarioHelper().recuperarUsuario(usuario, webServiceCallbackLogin);
            }
        });
        builder.show();
    }

    private void getCurrentAddress(){
        if(getAddressThread != null && getAddressThread.isAlive()){
            getAddressThread.interrupt();
            getAddressThread = null;
        }

        getAddressThread = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
                } catch (Exception e) {
                    LogHelper.error(e);
                }

                if (addresses == null || addresses.size()  == 0) {
                    return;
                }
                Address address = addresses.get(0);

                if(address.getMaxAddressLineIndex() > 0){
                    setMensagem(address.getAddressLine(0));
                    editTextEndereco.setText(address.getAddressLine(0));
                }else{
                    setMensagem("Sem endereço.");
                    editTextEndereco.setText("");
                }
            }
        });
        getAddressThread.start();
    }

    public class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent data) {
            if (data.hasExtra(Config.EXTRA_LOCATION)) {
                currentLocation = (Location) data.getExtras().get(Config.EXTRA_LOCATION);
                setMensagem("Você já pode pedir um mototaxi.");
                cancelarLocalizacao();
                exibirBotaoSolicitar(true);
                setUpMap();
                getCurrentAddress();
            }
        }
    }
}
