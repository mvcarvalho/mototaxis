package br.com.mototaxis.android.usuario.helpers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.util.Collection;

public class ObjectMapperHelper {

    private static ObjectMapper mapper;

    public static ObjectMapper getMapper(){
        if(mapper == null){
            mapper = getNewObjectMapper();
        }
        return mapper;
    }

    public static CollectionType getCollectionType(Class<? extends Collection> collectionClass, Class<?> elementClass){
        if(mapper == null){
            mapper = getNewObjectMapper();
        }
        return mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass);
    }

    private static ObjectMapper getNewObjectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        return mapper;
    }
}
