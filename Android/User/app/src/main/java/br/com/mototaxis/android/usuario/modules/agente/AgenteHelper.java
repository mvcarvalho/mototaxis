package br.com.mototaxis.android.usuario.modules.agente;

import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.usuario.helpers.WebServiceHelper;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class AgenteHelper {

    private static String GATEWAY = "agente";

    public void recuperarAgente(Agente agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "recuperarAgente", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void salvarAgente(Agente agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "salvarAgente", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }
}
