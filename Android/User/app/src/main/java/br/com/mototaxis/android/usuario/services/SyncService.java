package br.com.mototaxis.android.usuario.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import org.json.JSONException;

import br.com.mototaxis.android.usuario.Config;
import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.utils.WebServiceUtils;


/**
 * Created by Mateus Carvalho on 09/07/2015.
 */
public class SyncService extends Service {

    private Handler mainHandler;
    private Runnable mainRunnable;

    private WebServiceCallback novaCorridaServiceCallback;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initCallbacks();
        createService();
        return START_STICKY;
    }

    private void createService(){
        mainRunnable = new Runnable() {
            @Override
            public void run() {
                mainHandler.removeCallbacks(mainRunnable);
                execSyncTasks();
                mainHandler.postDelayed(mainRunnable, Config.UPDATE_INTERVAL);
            }
        };
        mainHandler = new Handler(this.getMainLooper());
        mainHandler.post(mainRunnable);
    }

    private void execSyncTasks(){

    }

    private void initCallbacks(){
        novaCorridaServiceCallback = new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                // TODO: 28/09/2015
                //Success

                sendUpdateBroadcast();
            }
            @Override
            public void onWebServiceError(String error) {
                try {
                    LogHelper.error(WebServiceUtils.getJsonError(error).getString("error"));
                } catch (JSONException e) {
                    LogHelper.error(e);
                }
            }
            @Override public void onError() {
                LogHelper.error("Sync Service call error.");
            }
        };
    }


    /**
     * Envia um broadcast das informações sensíveis à activity
     */
    private void sendUpdateBroadcast() {
        final Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Config.BROADCAST_SYNC_SERVICE_FINISH);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        this.sendBroadcast(broadcastIntent);
    }
}