package br.com.mototaxis.android.usuario.security;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Encrypter {

	public static final String KEY = "37e539214ae17650f6384350";
	public static final String IV_STRING = "73625395";

	private KeySpec				keySpec;
	private SecretKey			key;
	private IvParameterSpec		iv;

	public Encrypter() {
		this.init(Encrypter.KEY);
	}

	public Encrypter(final String key) {
		this.init(key);
	}

	private void init(final String keyString) {
		try {
			final MessageDigest md = MessageDigest.getInstance("md5");
			final byte[] digestOfPassword = md.digest(Base64.decodeBase64(keyString.getBytes("utf-8")));
			final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			for (int j = 0, k = 16; j < 8;) {
				keyBytes[k++] = keyBytes[j++];
			}

			this.keySpec = new DESedeKeySpec(keyBytes);

			this.key = SecretKeyFactory.getInstance("DESede").generateSecret(this.keySpec);

			this.iv = new IvParameterSpec(Encrypter.IV_STRING.getBytes());
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public String encrypt(String value) {
		try {
			final Cipher ecipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			ecipher.init(Cipher.ENCRYPT_MODE, this.key, this.iv);

			if (value == null) {
				return null;
			}

			// Encode the string into bytes using utf-8
			final byte[] utf8 = value.getBytes("UTF8");

			// Encrypt
			final byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			return new String(Base64.encodeBase64(enc), "UTF-8");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String decrypt(String value) {
		try {
			final Cipher dcipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			dcipher.init(Cipher.DECRYPT_MODE, this.key, this.iv);

			if (value == null) {
				return null;
			}

			// Decode base64 to get bytes
			final byte[] dec = Base64.decodeBase64(value.getBytes());

			// Decrypt
			final byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, "UTF8");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String decrypt64(String value) {
		try{
			final byte[] dec = Base64.decodeBase64(value.getBytes());
			return new String(dec, "UTF8");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}