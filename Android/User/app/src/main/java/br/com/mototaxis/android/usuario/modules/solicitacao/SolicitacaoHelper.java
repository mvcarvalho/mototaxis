package br.com.mototaxis.android.usuario.modules.solicitacao;

import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.usuario.helpers.WebServiceHelper;

/**
 * Created by Mateus Carvalho on 29/09/2015.
 */
public class SolicitacaoHelper {

    private static String GATEWAY = "solicitacao";

    public void recuperarSolicitacao(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "recuperarSolicitacao", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void iniciarSolicitacao(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "iniciarSolicitacao", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }

    public void cancelarSolicitacao(Solicitacao agente, WebServiceCallback callback){
        try{
            String params = ObjectMapperHelper.getMapper().writeValueAsString(agente);
            new WebServiceHelper().callWebService(GATEWAY, "cancelarSolicitacao", params, callback);
        }catch (Exception e){
            LogHelper.error(e);
        }
    }
}
