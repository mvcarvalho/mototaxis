package br.com.mototaxis.android.usuario;

import android.app.Application;

import br.com.mototaxis.android.usuario.helpers.SnappyDBHelper;
import br.com.mototaxis.android.usuario.modules.agente.Agente;
import br.com.mototaxis.android.usuario.modules.solicitacao.Solicitacao;
import br.com.mototaxis.android.usuario.modules.usuario.Usuario;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class MainApplication extends Application {

    private static MainApplication instance;

    public static MainApplication getInstance(){
        return instance;
    }

    private Agente agente;
    private Usuario usuario;
    private Solicitacao solicitacaoAtiva;

    @Override
    public void onCreate() {
        super.onCreate();

        agente = (Agente) SnappyDBHelper.getInstance().getObject(Config.SNAPPY_KEY_AGENTE, Agente.class, this);
        usuario = (Usuario) SnappyDBHelper.getInstance().getObject(Config.SNAPPY_KEY_USUARIO, Usuario.class, this);
        solicitacaoAtiva = (Solicitacao) SnappyDBHelper.getInstance().getObject(Config.SNAPPY_KEY_SOLICITACAO, Solicitacao.class, this);

        if(agente == null){
            agente = new Agente();
        }

        if(usuario == null){
            usuario = new Usuario();
        }

        if(solicitacaoAtiva == null){
            solicitacaoAtiva = new Solicitacao();
        }

        instance = this;
    }

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
        SnappyDBHelper.getInstance().storeObject(Config.SNAPPY_KEY_AGENTE, this.agente, this);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
        SnappyDBHelper.getInstance().storeObject(Config.SNAPPY_KEY_USUARIO, this.usuario, this);
    }

    public Solicitacao getSolicitacaoAtiva() {
        return solicitacaoAtiva;
    }

    public void setSolicitacaoAtiva(Solicitacao solicitacaoAtiva) {
        this.solicitacaoAtiva = solicitacaoAtiva;
    }
}
