package br.com.mototaxis.android.usuario.utils;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mateus on 07/07/2015.
 */
public class TextUtils {
    public static String formatDouble(final double valor, final int casasDecimais) {
        final NumberFormat numberFormatter = NumberFormat.getInstance(Locale.getDefault());
        numberFormatter.setMinimumFractionDigits(casasDecimais);
        numberFormatter.setMaximumFractionDigits(casasDecimais);
        return numberFormatter.format(valor);
    }

    public static String formatDate(final Date data) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return dateFormat.format(data);
    }

    public static String formatDate(long milliseconds) {
        return formatDate(new Date(milliseconds));
    }
}
