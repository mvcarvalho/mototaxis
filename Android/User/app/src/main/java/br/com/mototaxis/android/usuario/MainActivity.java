package br.com.mototaxis.android.usuario;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    protected void setFragment(int layout, Fragment fragment){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(layout, fragment);
        ft.commit();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.support.v7.appcompat.R.anim.abc_slide_in_bottom, android.support.v7.appcompat.R.anim.abc_slide_out_bottom);
    }

    public MainApplication getMainApplication(){
        return (MainApplication) getApplication();
    }

    public void showProgressBar(final String title, final String message, final boolean indeterminate){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progressDialog != null){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    progressDialog = null;
                }
                progressDialog = ProgressDialog.show(MainActivity.this, title, message, indeterminate, false);
            }
        });
    }

    public void updateProgressBar(final String title, final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progressDialog != null){
                    if(progressDialog.isShowing()){
                        progressDialog.setTitle(title);
                        progressDialog.setMessage(message);
                    }
                }
            }
        });
    }

    public void cancelProgressBar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progressDialog != null){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    progressDialog = null;
                }
            }
        });
    }

    public void showConfirmDialog(final int icon, final String title, final String message, final String positive, final String negative, final DialogInterface.OnClickListener positiveCallback, final DialogInterface.OnClickListener negativeCallback){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                if (icon > 0) {
                    builder.setIcon(icon);
                }
                if (title != null) {
                    builder.setTitle(title);
                }
                if (message != null) {
                    builder.setMessage(message);
                }
                if (positive != null) {
                    builder.setPositiveButton(positive, positiveCallback);
                }
                if (negative != null) {
                    builder.setNegativeButton(negative, negativeCallback);
                }

                builder.show();
            }
        });
    }
}
