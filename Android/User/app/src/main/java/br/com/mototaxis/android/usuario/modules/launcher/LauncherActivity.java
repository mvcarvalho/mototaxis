package br.com.mototaxis.android.usuario.modules.launcher;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import br.com.mototaxis.android.usuario.MainActivity;
import br.com.mototaxis.android.usuario.R;
import br.com.mototaxis.android.usuario.helpers.DataHelper;
import br.com.mototaxis.android.usuario.helpers.GcmHelper;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.modules.map.MapActivity;

/**
 * Created by Mateus Carvalho on 28/09/2015.
 */
public class LauncherActivity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        //registerGcm();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(LauncherActivity.this, MapActivity.class));
                finish();
            }
        }, 1000);
    }

    private void registerGcm() {
        new AsyncTask<Void, Void, Void>(){
            protected Void doInBackground(final Void... params) {
                final DataHelper dataHelper = new DataHelper();
                if(!dataHelper.getGcmToken(LauncherActivity.this).equals("")){
                    LogHelper.error(dataHelper.getGcmToken(LauncherActivity.this));
                    return null;
                }

                GcmHelper gcmHelper = new GcmHelper();
                String token = gcmHelper.registerGcm(LauncherActivity.this);
                dataHelper.setGcmToken(token, LauncherActivity.this);

                return null;
            }
        }.execute();
    }
}
