package br.com.mototaxis.android.usuario.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


import java.io.IOException;

import br.com.mototaxis.android.usuario.Config;
import br.com.mototaxis.android.usuario.MainApplication;
import br.com.mototaxis.android.usuario.callbacks.WebServiceCallback;
import br.com.mototaxis.android.usuario.helpers.LogHelper;
import br.com.mototaxis.android.usuario.helpers.ObjectMapperHelper;
import br.com.mototaxis.android.usuario.modules.agente.Agente;
import br.com.mototaxis.android.usuario.modules.agente.AgenteHelper;


/**
 * Created by Mateus Carvalho on 09/07/2015.
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    private Location lastLocation;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(Config.UPDATE_LOCATION_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(0);

        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this);
        builder.addApi(LocationServices.API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);

        googleApiClient = builder.build();
        googleApiClient.connect();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if(googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
        super.onDestroy();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(location.getAccuracy() > 100){
            return;
        }
        lastLocation  = location;
        sendCurrentLocation();
        sendUpdateBroadcast();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * Envia um broadcast das informações sensíveis à activity
     */
    private void sendUpdateBroadcast() {
        final Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra(Config.EXTRA_LOCATION, lastLocation);
        broadcastIntent.setAction(Config.BROADCAST_LOCATION_RECEIVED);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        this.sendBroadcast(broadcastIntent);
    }

    private void sendCurrentLocation(){
        Agente agente = ((MainApplication)getApplication()).getAgente();

        if(agente.getId() <= 0){
            return;
        }

        agente.setLatitude(lastLocation.getLatitude());
        agente.setLongitude(lastLocation.getLongitude());
        new AgenteHelper().salvarAgente(agente, new WebServiceCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    Agente agente = ObjectMapperHelper.getMapper().readValue(result, Agente.class);
                    if(agente != null && agente.getId() > 0){
                        ((MainApplication)getApplication()).setAgente(agente);
                    }
                } catch (IOException e) {
                    LogHelper.error(e);
                }
            }

            @Override
            public void onWebServiceError(String error) {
                LogHelper.error(error);
            }

            @Override
            public void onError() {
                LogHelper.error("Falha ao enviar dados ao servidor.");
            }
        });
    }
}