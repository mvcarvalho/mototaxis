package br.com.mototaxis.android.usuario.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.mototaxis.android.usuario.Config;


/**
 * Created by Mateus Carvalho on 21/04/2015.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    public SQLiteHelper(Context context) {
        super(context, Config.DATABASE_NAME, null, Config.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public Cursor select(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        this.openDatabase();
        Cursor cursor = this.database.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
        return cursor;
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        this.openDatabase();
        long id = this.database.insert(table, nullColumnHack, values);
        this.closeDatabase();
        return id;
    }

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        this.openDatabase();
        int rowsAffected = this.database.update(table, values, whereClause, whereArgs);
        this.closeDatabase();
        return rowsAffected;
    }

    public int delete(String table, String whereClause, String[] whereArgs) {
        this.openDatabase();
        int rowsAffected = this.database.delete(table, whereClause, whereArgs);
        this.closeDatabase();
        return rowsAffected;
    }

    private void openDatabase() {
        if (this.database == null) {
            this.database = this.getWritableDatabase();
        }
    }

    public void closeDatabase() {
        if (this.database != null) {
            this.database.close();
            this.database = null;
        }
    }
}
