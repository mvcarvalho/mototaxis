package br.com.mototaxis.android.usuario.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import br.com.mototaxis.android.usuario.R;

/**
 * Created by Mateus Carvalho on 06/08/2015.
 */
public class NotificationUtils {
    public static void openNotification(Context context, Intent resultIntent, int notificationId, String notificationTag, String title, String message){
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(message);
        mBuilder.setTicker("Novidade chegando!");
        mBuilder.setContentIntent(resultPendingIntent);

        Notification noti = mBuilder.build();
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        //noti.flags |= Notification.FLAG_NO_CLEAR;

        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(notificationTag, notificationId, noti);
    }

    public static void closeAllNotifications(Context context){
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.cancelAll();
    }

    public static void closeNotification(Context context, String notificationTag, int notificationId){
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(notificationTag, notificationId);
    }
}
