'use strict';

var systemApp = angular.module('systemApp',
    [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'lr.upload',
        'md.data.table'
    ]
);