'use strict';

systemApp.factory('SessionService',
    [
        '$window',
        function ($window) {
            var store = $window.sessionStorage;

            var add = function (key, value) {
                value = $window.sjcl.encrypt("d346ed54a5d2c022", angular.toJson(value));
                value = angular.toJson(value);
                store.setItem(key, value);
            };

            var get = function (key) {
                var value = store.getItem(key);
                if (value) {
                    value = $window.sjcl.decrypt("d346ed54a5d2c022", angular.fromJson(value));
                    value = angular.fromJson(value);
                }
                return value;
            };

            var remove = function (key) {
                store.removeItem(key);
            };

            return {
                add: add,
                get: get,
                remove: remove
            };
        }
    ]
);
