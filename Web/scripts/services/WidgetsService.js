'use strict';

systemApp.factory('WidgetsService',
    [
        '$rootScope',
        '$timeout',
        '$mdToast',
        function ($rootScope, $timeout, $mdToast) {

            var closeProgressBarTimeout = undefined;

            var showProgressBar = function(show){
                if(show){
                    $rootScope.loading = true;
                }else{
                    if(closeProgressBarTimeout){
                        $timeout.cancel(closeProgressBarTimeout);
                    }
                    closeProgressBarTimeout = $timeout(function(){
                        $rootScope.loading = false;
                    }, 700);
                }
            };

            var showToastAction = function(message, action, callback){
                var toast = $mdToast.simple().content(message).action(action).highlightAction(true).position('top right');
                $mdToast.show(toast).then(function(response) {
                    if(response == 'ok' && callback) {
                        callback();
                    }
                });
            };

            var showToast = function(message){
                var toast = $mdToast.simple().content(message).position('top right');
                $mdToast.show(toast);
            };

            return {
                showProgressBar: showProgressBar,
                showToastAction: showToastAction,
                showToast: showToast
            };
        }
    ]
);
