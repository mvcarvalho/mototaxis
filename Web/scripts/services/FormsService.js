'use strict';

systemApp.factory('FormsService',
    [
        '$window',
        function ($window) {

            var applyMasks = function(){
                $window.$('.cnpj').mask('00.000.000/0000-00');
                $window.$('.cpf').mask('000.000.000-00');
                $window.$('.phone').mask('(00)0000-00000');
                $window.$('.number').mask('000.000');
            };

            return {
                applyMasks: applyMasks
            };
        }
    ]
);
