'use strict';

systemApp.factory('WebService',
    [
        '$http',
        'WidgetsService',
        'ConstantsService',
        'upload',
        function ($http, WidgetsService, ConstantsService, upload) {

            var call = function (gateway, method, param, success, error) {
                WidgetsService.showProgressBar(true);
                var request = {
                    url: ConstantsService.SERVER_URL + gateway,
                    method: 'POST',
                    headers: {'Content-Type': 'text/plain; charset=UTF-8'},
                    cache: false,
                    data: {
                        method: method,
                        params: JSON.stringify(param),
                        date: new Date().getTime()
                    }
                };
                var processData = function (data) {
                    if (data.data.hasOwnProperty('error') || data.data.hasOwnProperty('errorcode')) {
                        if (error) {
                            console.log(data.data);
                            error(data.data);
                        }
                    } else {
                        if (success) {
                            success(data.data);
                        }
                    }

                    WidgetsService.showProgressBar(false);
                };
                var processError = function (data) {
                    var response = data;
                    if(!response){
                        response = {
                            errorcode: '00000',
                            erro: 'Impossível acessar o servidor.'
                        };
                    }

                    console.log(response);

                    if (error) {
                        error(response);
                    }
                    WidgetsService.showProgressBar(false);
                };

                $http(request).then(processData, processError);
            };

            var callUpload = function (file, method, success, error) {
                var requestUpload = {
                    url: ConstantsService.SERVER_URL + 'uploads',
                    method: 'POST',
                    data: {
                        method: method,
                        file: file
                    }
                };
                var processData = function (data) {
                    var response = data.data;
                    if (response.hasOwnProperty('error') || response.hasOwnProperty('errorcode')) {
                        if (error) {
                            error(response);
                        }
                        console.log('Erro na solicitação: ' + method);
                        console.log(response);
                    } else {
                        if (success) {
                            success(response);
                        }
                    }
                };
                var processError = function () {
                    var response = {
                        errorcode: '00000',
                        erro: 'Impossível acessar o servidor.'
                    };
                    if (error) {
                        error(response);
                    }
                };
                upload(requestUpload).then(processData, processError);
            };

            return {
                call: call,
                callUpload: callUpload
            };
        }
    ]
);
