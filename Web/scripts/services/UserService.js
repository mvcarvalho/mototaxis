'use strict';

systemApp.factory('UserService',
    [
        'SessionService',
        function (SessionService) {

            var USER_KEY = 'USPASHGHAJIODNAYINM';
            var callbacks = [];

            var addCallback = function(callback){
                if(callback){
                    callbacks[callbacks.length] = callback;
                }
            };

            var notifyCallbacks = function(user){
                for(var x = 0; x < callbacks.length; x++){
                    callbacks[x](user);
                }
            };

            var saveUser = function(user){
                SessionService.add(USER_KEY, user);
                notifyCallbacks(user);
            };

            var getUser = function(){
                return SessionService.get(USER_KEY);
            };

            return{
                addCallback: addCallback,
                saveUser: saveUser,
                getUser: getUser
            }
        }
    ]
);
