'use strict';

systemApp.factory('FilesService',
    [
        '$window',
        function ($window) {

            var downloadFile = function(url){
                var hiddenElement = $window.document.createElement('a');
                hiddenElement.setAttribute('href', url);
                hiddenElement.setAttribute('target', '_blank');
                hiddenElement.click();
            };

            var downloadCanvas = function(canvasName){
                var canvas = $window.document.getElementById(canvasName);
                $window.Canvas2Image.saveAsImage(canvas, canvas.width, canvas.height, 'jpg');
            };

            return {
                downloadFile: downloadFile,
                downloadCanvas: downloadCanvas
            };
        }
    ]
);
