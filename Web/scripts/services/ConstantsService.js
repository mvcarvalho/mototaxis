'use strict';

systemApp.factory('ConstantsService',
    [
        function () {
            return {
                SERVER_URL: 'http://ec2-54-210-131-63.compute-1.amazonaws.com:8080/WebServices/',
                //SERVER_URL: 'http://localhost:8080/',

                AGENTE_GATEWAY: 'agente',
                AGENTE_METHOD_SALVAR: 'salvarAgente',
                AGENTE_METHOD_LISTAR: 'listarAgentes',
                AGENTE_METHOD_LOGIN: 'efetuarLogin',

                SOLICITACAO_GATEWAY: 'solicitacao',
                SOLICITACAO_METHOD_RECUPERAR: 'recuperarSolicitacao',
                SOLICITACAO_METHOD_LISTAR: 'listarSolicitacoes'
            };
        }
    ]
);
