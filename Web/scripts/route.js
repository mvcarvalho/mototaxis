'use strict';

systemApp.config(
    [
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/agente', {
                    templateUrl: 'modules/agente/agente.html',
                    controller: 'AgenteCtrl'
                })
                .when('/agentetaxi', {
                    templateUrl: 'modules/agentetaxi/agentetaxi.html',
                    controller: 'AgenteTaxiCtrl'
                })
                .when('/login', {
                    templateUrl: 'modules/login/login.html',
                    controller: 'LoginCtrl'
                })
                .when('/dashboard', {
                    templateUrl: 'modules/dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                })
                .otherwise({
                    redirectTo: '/login'
                });
        }
    ]
);
