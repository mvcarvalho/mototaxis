'use strict';

systemApp.run(
    [
        '$rootScope',
        '$location',
        'Strings',
        'UserService',
        function ($rootScope, $location, Strings, UserService) {
            Strings.loadStrings('pt-br');

            $rootScope.$on('$routeChangeStart', function (event, next) {
                var user = UserService.getUser();
                if (!user || !user.id || user.id <= 0) {
                    if (next.templateUrl != "modules/login/login.html") {
                        console.log('Acesso negado!');
                        event.preventDefault();
                        $location.path("/login");
                    }
                }
            });
        }
    ]
);