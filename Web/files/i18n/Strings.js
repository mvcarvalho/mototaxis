'use strict';

systemApp.factory('Strings', [
    '$http',
    '$rootScope',
    function ($http, $rootScope) {
        var langDefault = 'pt-br';

        var loadStrings = function(newLang){

            if(!newLang || newLang == ''){
                newLang = langDefault;
            }

            $http.get('i18n/lang-'+newLang+'.json')
                .success(function (data) {
                    $rootScope.strings = data;
                })
                .error(function(){
                    if(newLang != langDefault){
                        loadStrings(langDefault);
                    }
                });
        };

        return {
            loadStrings: loadStrings
        }
    }
]);