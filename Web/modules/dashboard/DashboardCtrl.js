'use strict';

systemApp.controller('DashboardCtrl',
    [
        '$scope',
        'ConstantsService',
        'WebService',
        function ($scope, ConstantsService, WebService) {


            var listarSolicitacoesAbertoSuccess = function(data){
                console.log(data);
                $scope.solicitacoes = data;
            };
            var listarSolicitacoesAbertoError = function(){
                $scope.solicitacoes = [];
            };
            var listarSolicitacoesAberto = function(){
                WebService.call(ConstantsService.SOLICITACAO_GATEWAY, ConstantsService.SOLICITACAO_METHOD_LISTAR, '', listarSolicitacoesAbertoSuccess, listarSolicitacoesAbertoError);
            };

            var listarAgentesSuccess = function(data){
                $scope.agentes = data;
            };
            var listarAgentesError = function(){
                $scope.agentes = [];
            };
            var listarAgentes = function(){
                WebService.call(ConstantsService.AGENTE_GATEWAY, ConstantsService.AGENTE_METHOD_LISTAR, '', listarAgentesSuccess, listarAgentesError);
            };

            $scope.solicitacoes = [];
            $scope.agentes = [];
            $scope.query = {order: 'data'};

            listarSolicitacoesAberto();
            listarAgentes();
        }
    ]
);