'use strict';

systemApp.controller('LoginCtrl',
    [
        '$scope',
        '$location',
        'WidgetsService',
        'UserService',
        function ($scope, $location, WidgetsService, UserService) {

            var onClickLogin = function(){
                if($scope.user && $scope.user.login == 'admin' && $scope.user.senha == '88motos2015'){
                    $scope.user.id = 999;
                    UserService.saveUser($scope.user);
                    $location.path('/agente');
                }else{
                    WidgetsService.showToast('Usuário ou senha inválidos');
                }
            };

            $scope.onClickLogin = onClickLogin;
        }
    ]
);