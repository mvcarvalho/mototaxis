'use strict';

systemApp.controller('AgenteTaxiCtrl',
    [
        '$scope',
        'ConstantsService',
        'WebService',
        'WidgetsService',
        '$window',
        function ($scope, ConstantsService, WebService, WidgetsService, $window) {

            var onClickSalvarAgenteSuccess = function(){
                WidgetsService.showToast('Agente Cadastrado');
                $scope.agente = undefined;
                listarAgentes();
            };
            var onClickSalvarAgenteError = function(error){
                WidgetsService.showToastAction(error.error, 'Ok');
            };
            var onClickSalvarAgente = function(){
                if($scope.arquivoCamera && !$scope.nomeArquivo){
                    enviarArquivo($scope.arquivoCamera);
                    return;
                }
                $scope.agente.foto = $scope.nomeArquivo;

                WebService.call(ConstantsService.AGENTE_GATEWAY, ConstantsService.AGENTE_METHOD_SALVAR, $scope.agente, onClickSalvarAgenteSuccess, onClickSalvarAgenteError);
            };

            var listarAgentesSuccess = function(data){
                $scope.agentes = data;
            };
            var listarAgentesError = function(error){
                WidgetsService.showToastAction(error.error, 'Ok');
            };
            var listarAgentes = function(){
                WebService.call(ConstantsService.AGENTE_GATEWAY, ConstantsService.AGENTE_METHOD_LISTAR, {tipo: 'T'}, listarAgentesSuccess, listarAgentesError);
            };

            var selecionarAgente = function(agente){
                $scope.agente = agente;
            };

            var onClickNovoAgente = function(){
                setNewAgente();
            };

            var setNewAgente = function(){
                $scope.agente = {
                    nome: '',
                    email: '',
                    login: '',
                    senha: '',
                    alvara: '',
                    telefone1: '',
                    telefone2: '',
                    modelocarro: '',
                    corcarro: '',
                    placacarro: '',
                    fotomototaxista: '',
                    cpf: '',
                    rg: '',
                    estado: '',
                    cidade: '',
                    endereco: '',
                    tipo: 'T',
                    latitude: 0.0,
                    longitude: 0.0,
                    precoviagem: 0.0,
                    taxaviagem: 1.0
                };
            };

            var CSSayCheese = null;
            var CSSayCheeseContent = null;
            var enableCamCameraScripts = function() {
                if(!CSSayCheese){
                    CSSayCheese = new SayCheese('#webcampreview', {snapshots: true});
                    CSSayCheeseContent = $window.document.getElementById('webcampreview');
                }

                CSSayCheese.on('start', function () {
                    $scope.capturar = true;
                    $scope.camera = true;
                    $scope.$apply();
                });

                CSSayCheese.on('error', function (error) {
                    $scope.camera = false;
                    $scope.capturar = false;
                    WidgetsService.showToastAction(error.error, 'Ok');
                });

                CSSayCheese.on('snapshot', function (snapshot) {
                    $scope.capturar = false;
                    $scope.arquivoCamera = snapshot.toDataURL('image/png');
                    CSSayCheese.stop();
                });

                if(CSSayCheeseContent.innerHTML){
                    CSSayCheeseContent.innerHTML = '';
                }

                CSSayCheese.start();
            };
            var capturarImagem = function(){
                if(CSSayCheese){
                    CSSayCheese.takeSnapshot();
                }
            };

            var enviarArquivoSuccess = function(filename){
                $scope.nomeArquivo = filename;
                onClickSalvarAgente();
            };
            var enviarArquivoError = function(arquivo, error){
                $window.NProgress.done();
                WidgetsService.showToastAction(error.error, 'Ok');
            };
            var enviarArquivo = function(arquivo){
                $scope.nomeArquivo = undefined;
                WebService.callUpload(arquivo, 'salvarFotoAgente', enviarArquivoSuccess, function (error) {enviarArquivoError(arquivo, error);});
            };

            $scope.agentes = [];
            $scope.arquivoCamera = undefined;
            $scope.nomeArquivo = undefined;
            $scope.camera = false;

            $scope.onClickSalvarAgente = onClickSalvarAgente;
            $scope.selecionarAgente = selecionarAgente;
            $scope.onClickNovoAgente = onClickNovoAgente;
            $scope.enableCamCameraScripts = enableCamCameraScripts;
            $scope.capturarImagem = capturarImagem;

            listarAgentes();
        }
    ]
);