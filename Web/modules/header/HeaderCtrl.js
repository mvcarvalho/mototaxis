'use strict';

systemApp.controller('HeaderCtrl',
    [
        '$scope',
        '$location',
        'UserService',
        function ($scope, $location, UserService) {

            var showHeader = function(){
                if(UserService.getUser().id && UserService.getUser().id > 0){
                    return true;
                }else{
                    return false;
                }
            };

            var onClickLogout = function(){
                UserService.saveUser({});
                $location.path('/login');
            };

            $scope.showHeader = showHeader;
            $scope.onClickLogout = onClickLogout;
        }
    ]
);
